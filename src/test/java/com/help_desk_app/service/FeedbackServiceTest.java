package com.help_desk_app.service;

import com.help_desk_app.converter.FeedbackConverter;
import com.help_desk_app.dao.FeedbackDao;
import com.help_desk_app.dto.FeedbackDto;
import com.help_desk_app.entity.Feedback;
import com.help_desk_app.entity.Ticket;
import com.help_desk_app.entity.User;
import com.help_desk_app.entity.enums.Category;
import com.help_desk_app.entity.enums.Rate;
import com.help_desk_app.entity.enums.Role;
import com.help_desk_app.entity.enums.Urgency;
import com.help_desk_app.exception.NotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import java.util.Date;
import java.util.Optional;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FeedbackServiceTest {
    private Feedback feedback;
    private FeedbackDto feedbackDto;

    @Mock
    private FeedbackDao feedbackDaoMock;

    @Mock
    private FeedbackConverter feedbackConverterMock;

    @InjectMocks
    private FeedbackService feedbackService;

    @Before
    public void setUp() {
        User user = new User("Sergey", "Petrov", Role.EMPLOYEE,
                "petrov@mail.ru", "string");
        Ticket ticket = new Ticket("Ticket", "des", new Date(),
                Category.APPLICATION_AND_SERVERS, Urgency.AVERAGE);
        ticket.setId(3L);
        feedback = new Feedback(user, Rate.FOUR, new Date(), "text", ticket);
        feedback.setId(3L);
        feedbackDto = new FeedbackDto((byte) 4, "text", 3L);
    }

    @Test
    public void testFindOneByTicketId() {
        when(feedbackDaoMock.findOneByTicketId(3L)).thenReturn(Optional.ofNullable(feedback));
        when(feedbackConverterMock.toDto(feedback)).thenReturn(feedbackDto);
        FeedbackDto expected = feedbackDto;
        FeedbackDto actual = feedbackService.findOneByTicketId(3L);
        assertEquals(expected, actual);
    }

    @Test(expected =  NotFoundException.class)
    public void testFindOneByTicketIdNotFound() {
        when(feedbackDaoMock.findOneByTicketId(5L)).thenThrow(new NotFoundException(Feedback.class, 3L));
        FeedbackDto actual = feedbackService.findOneByTicketId(5L);
    }

    @Test
    public void testCreate() {
        when(feedbackDaoMock.create(feedback)).thenReturn(feedback);
        when(feedbackConverterMock.fromDto(feedbackDto)).thenReturn(feedback);
        when(feedbackConverterMock.toDto(feedback)).thenReturn(feedbackDto);
        FeedbackDto expected = feedbackDto;
        FeedbackDto actual = feedbackService.create(feedbackDto);
        assertEquals(expected, actual);
    }
}