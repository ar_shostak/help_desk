package com.help_desk_app.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.help_desk_app.config.hibernate.HibernateUtils;
import com.help_desk_app.config.security.WebSecurityConfig;
import com.help_desk_app.config.web.WebConfig;
import com.help_desk_app.dto.FeedbackDto;
import com.help_desk_app.service.FeedbackService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateUtils.class, WebConfig.class, WebSecurityConfig.class})
@WebAppConfiguration(value = "src/main/java/com/help_desk_app/config")
public class FeedbackControllerTest {
    private static final String GET_FEEDBACK_URL = "/feedback/4";
    private static final String LEAVE_FEEDBACK_URL = "/feedback";
    private static final String CONTENT_TYPE = "application/json";

    private MockMvc mockMvc;
    private MockHttpSession sessionMock;
    private FeedbackDto feedbackDto;

    @Mock
    private FeedbackService feedbackServiceMock;

    @InjectMocks
    private FeedbackController feedbackController;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(feedbackController).build();
        sessionMock = new MockHttpSession();
        feedbackDto = new FeedbackDto((byte) 4, "text", 6L);
    }

    @Test
    public void testGet() throws Exception {
        when(feedbackServiceMock.findOneByTicketId(4L)).thenReturn(feedbackDto);
        mockMvc.perform(get(GET_FEEDBACK_URL))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"rate\":4,\"text\":\"text\",\"ticketId\":6}"));
    }

    @Test
    public void create() throws Exception {
        when(feedbackServiceMock.create(feedbackDto)).thenReturn(feedbackDto);
        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String requestJson = objectWriter.writeValueAsString(feedbackDto);
        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.post((LEAVE_FEEDBACK_URL))
                        .contentType(CONTENT_TYPE)
                        .accept(CONTENT_TYPE)
                        .content(requestJson)
                        .session(sessionMock);
        mockMvc.perform(builder)
                .andExpect(status().isCreated())
                .andExpect(content().string("{\"rate\":4,\"text\":\"text\",\"ticketId\":6}"));
    }
}