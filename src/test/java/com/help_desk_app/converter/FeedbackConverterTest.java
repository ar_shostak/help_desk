package com.help_desk_app.converter;

import com.help_desk_app.dto.FeedbackDto;
import com.help_desk_app.entity.Feedback;
import com.help_desk_app.entity.Ticket;
import com.help_desk_app.entity.User;
import com.help_desk_app.entity.enums.Category;
import com.help_desk_app.entity.enums.Rate;
import com.help_desk_app.entity.enums.Role;
import com.help_desk_app.entity.enums.Urgency;
import com.help_desk_app.exception.NotFoundException;
import com.help_desk_app.service.AuthenticationService;
import com.help_desk_app.service.TicketService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import java.util.Date;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FeedbackConverterTest {
    private Feedback feedback;
    private FeedbackDto feedbackDto;
    private User user;
    private Ticket ticket;

    @Mock
    private TicketService ticketServiceMock;

    @Mock
    private AuthenticationService authenticationServiceMock;

    @InjectMocks
    private FeedbackConverter feedbackConverter;

    @Before
    public void setUp() {
        user = new User("Michail", "Shemelev", Role.EMPLOYEE,
                "pgs@mail.ru", "string");
        ticket = new Ticket("Ticket", "description", new Date(),
                Category.APPLICATION_AND_SERVERS, Urgency.AVERAGE);
        ticket.setId(6L);
        feedback = new Feedback(user, Rate.FOUR, new Date(), "sometext", ticket);
        feedbackDto = new FeedbackDto((byte) 4, "sometext", 6L);
    }

    @Test
    public void testToDto() {
        FeedbackDto expected = feedbackDto;
        FeedbackDto actual = feedbackConverter.toDto(feedback);
        assertEquals(expected, actual);
    }

    @Test
    public void testFromDto() {
        when(authenticationServiceMock.getAuthenticateUser()).thenReturn(user);
        when(ticketServiceMock.getTicketById(6L)).thenReturn(ticket);
        Feedback expected = feedback;
        Feedback actual = feedbackConverter.fromDto(feedbackDto);
        assertEquals(expected.getText(), actual.getText());
        assertEquals(expected.getRate(), actual.getRate());
    }

    @Test(expected =  NotFoundException.class)
    public void testFromDtoWrong() {
        when(authenticationServiceMock.getAuthenticateUser()).thenReturn(user);
        FeedbackDto dto = new FeedbackDto((byte) 8, "sometext", 6L);
        Feedback actual = feedbackConverter.fromDto(dto);
    }

    @Test(expected =  NotFoundException.class)
    public void testFromDtoNullRate() {
        when(authenticationServiceMock.getAuthenticateUser()).thenReturn(user);
        FeedbackDto dto = new FeedbackDto(null, "sometext", 6L);
        Feedback actual = feedbackConverter.fromDto(dto);
    }
}