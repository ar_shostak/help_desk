package com.help_desk_app.utils;

import com.help_desk_app.entity.Feedback;
import com.help_desk_app.entity.Ticket;
import com.help_desk_app.entity.User;
import com.help_desk_app.entity.enums.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;

import static junit.framework.TestCase.assertEquals;

@RunWith(Parameterized.class)
public class ActionCreatorUtilTest {
    private static User user;
    private static Ticket ticket;

    private Feedback feedback;
    private Set<String> actions;
    private Role role;
    private State state;
    private Long id;

    @Parameterized.Parameters
    public static Collection<Object[]> testDate() {
        return Arrays.asList(new Object[][]{
                {new HashSet<>(Arrays.asList("Submit", "Cancel")), Role.EMPLOYEE,  State.DRAFT, null, 1L},
                {new HashSet<>(Arrays.asList("Submit", "Cancel")), Role.EMPLOYEE,  State.DECLINED, null, 1L},
                {Collections.singleton("View Feedback"), Role.EMPLOYEE,  State.DONE, new Feedback(), 1L},
                {new HashSet<>(Arrays.asList("Leave Feedback")), Role.EMPLOYEE,  State.DONE, null, 1L},
                {new HashSet<>(Arrays.asList("Submit", "Cancel")), Role.MANAGER,  State.DRAFT, null, 1L},
                {new HashSet<>(Arrays.asList("Submit", "Cancel")), Role.MANAGER,  State.DECLINED, null, 1L},
                {new HashSet<>(Arrays.asList("Decline", "Cancel", "Approve")), Role.MANAGER,  State.NEW, null, 1L},
                {Collections.singleton("View Feedback"), Role.MANAGER,  State.DONE, new Feedback(), 1L},
                {new HashSet<>(Arrays.asList("View Feedback", "Leave Feedback")), Role.MANAGER,  State.DONE, null, 1L},
                {new HashSet<>(Arrays.asList("Assign to me", "Cancel")), Role.ENGINEER,  State.APPROVED, null, 1L},
                {Collections.singleton("Done"), Role.ENGINEER,  State.IN_PROGRESS, null, 1L},
                {Collections.singleton("View Feedback"), Role.ENGINEER,  State.DONE, null, 1L},
        });
    }

    public ActionCreatorUtilTest(Set<String> actions, Role role, State state, Feedback feedback, Long id){
        this.role = role;
        this.state = state;
        this.feedback = feedback;
        this.actions = actions;
        this.id = id;
    }

    @BeforeClass
    public static void CreateUsersAndTickets() {
        user = new User("Sergey", "Petrov", Role.EMPLOYEE,
                "petrov@mail.ru", "string");
        ticket = new Ticket("Ticket","des", new Date(),
                Category.APPLICATION_AND_SERVERS, Urgency.AVERAGE);
    }

    @Test
    public void testGetActions() {
        user.setRole(role);
        user.setId(id);
        ticket.setState(state);
        ticket.setUser(user);
        ticket.setFeedback(feedback);
        Set<String> expected = actions;
        Set<String> actual = ActionCreatorUtil.getActions(user, ticket);
        assertEquals(expected, actual);
    }
}