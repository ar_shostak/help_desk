package com.help_desk_app.dao;

import com.help_desk_app.entity.Ticket;
import java.util.List;

public interface TicketDao extends BaseDao<Ticket> {
    List<Ticket> getAllForTickets();
    List<Ticket> getAllByUserId(Long userId);
    List<Ticket> getAllWithStateNew();
    List<Ticket> getAllWithStateApproved();
    List<Ticket> getAllByApproverId(Long approverId);
    List<Ticket> getAllByAssigneeId(Long assigneeId);

}
