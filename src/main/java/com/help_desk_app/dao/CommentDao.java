package com.help_desk_app.dao;

import com.help_desk_app.entity.Comment;
import java.util.List;

public interface CommentDao extends BaseDao<Comment> {
   List<Comment> findByTicketId(Long id);
}
