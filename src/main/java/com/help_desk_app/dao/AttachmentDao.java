package com.help_desk_app.dao;

import com.help_desk_app.entity.Attachment;

public interface AttachmentDao extends BaseDao<Attachment> {
}
