package com.help_desk_app.dao.impl;

import com.help_desk_app.dao.UserDao;
import com.help_desk_app.entity.User;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {
    private static final String SELECT_USER_BY_NAME = "from User WHERE email = :email";
    private static final String NAME_COLUMN = "email";

    private Logger logger = Logger.getLogger(this.getClass());

    private SessionFactory sessionFactory;

    public UserDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        logger.debug(String.format("Try to find User with email %s", email));
        Optional<User> user;
        try(Session session = sessionFactory.openSession()) {
            user = session.createQuery(SELECT_USER_BY_NAME, User.class).setParameter(NAME_COLUMN, email).uniqueResultOptional();
        }
        logger.debug(String.format("User was found %s", user));
        return user;
    }
}
