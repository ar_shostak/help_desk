package com.help_desk_app.dao.impl;

import com.help_desk_app.entity.Ticket;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class HistoryDaoImpl extends EnversDaoImpl<Ticket> {

    public HistoryDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
