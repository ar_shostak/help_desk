package com.help_desk_app.dao.impl;

import com.help_desk_app.dao.FeedbackDao;
import com.help_desk_app.entity.Feedback;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public class FeedbackDaoImpl extends BaseDaoImpl<Feedback> implements FeedbackDao {
    private static final String TICKET_COLUMN = "ticket_id";
    private static final String SELECT_FEEDBACK_SQL_STATEMENT = "from Feedback where ticket_id = :ticket_id";

    private Logger logger = Logger.getLogger(this.getClass());

    public FeedbackDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
     }

    @Override
    public Optional<Feedback> findOneByTicketId(Long id) {
        logger.debug(String.format("Trying to find Feedback for Ticket with id%s", id));
        Optional<Feedback> feedback = (getSession().createQuery(SELECT_FEEDBACK_SQL_STATEMENT, Feedback.class)
                .setParameter(TICKET_COLUMN, id).uniqueResultOptional());
        logger.info(String.format("Feedback was found %s", feedback));
        return feedback;
    }
}

