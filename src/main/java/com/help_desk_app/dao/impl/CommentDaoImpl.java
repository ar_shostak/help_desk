package com.help_desk_app.dao.impl;

import com.help_desk_app.dao.CommentDao;
import com.help_desk_app.entity.Comment;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class CommentDaoImpl extends BaseDaoImpl<Comment> implements CommentDao {
    private static final String COMMENT_COLUMN = "ticket_id";
    private static final String SELECT_COMMENT_SQL_STATEMENT = "from Comment where ticket_id = :ticket_id";

    private Logger logger = Logger.getLogger(this.getClass());

    public CommentDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<Comment> findByTicketId(Long id) {
        logger.debug(String.format("Try to find Comments for Ticket with id = %s", id));
        List<Comment> comments = (getSession().createQuery(SELECT_COMMENT_SQL_STATEMENT, Comment.class)
                .setParameter(COMMENT_COLUMN, id).getResultList());
        logger.info(String.format("Comments were found %s", comments));
        return comments;
    }
}
