package com.help_desk_app.dao.impl;

import com.help_desk_app.dao.EnversDao;
import com.help_desk_app.history.Revision;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class EnversDaoImpl<T> extends BaseDaoImpl<T> implements EnversDao<T> {
    private final static String EMPTY = "";
    private Logger logger = Logger.getLogger(this.getClass());
    private final Class<T> aClass;

    public EnversDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.aClass = (Class<T>)((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public List<Revision<T>> getRevisionsById(long id) {
        return  convertToRevisions(getAuditQuery(id).getResultList());
    }

    private AuditQuery getAuditQuery(long id) {
        return AuditReaderFactory.get(getSession()).createQuery()
                .forRevisionsOfEntity(aClass, false, true)
                .add(AuditEntity.id().eq(id))
                .addOrder(AuditEntity.revisionNumber().asc());
    }

    private List<Revision<T>> convertToRevisions(List<Object[]> list) {
        logger.debug(String.format("Creating Revisions from %s", list == null ? EMPTY : list ));
        return list.stream().map((Function<Object[], Revision<T>>) Revision::new)
                .peek(revision -> logger.info(String.format("Revision was created : %s", revision)))
                .collect(Collectors.toList());
    }
}