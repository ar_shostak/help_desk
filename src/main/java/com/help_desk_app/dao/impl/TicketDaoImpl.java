package com.help_desk_app.dao.impl;

import com.help_desk_app.dao.TicketDao;
import com.help_desk_app.entity.Ticket;
import com.help_desk_app.entity.enums.State;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import java.util.Arrays;
import java.util.List;
import static com.help_desk_app.entity.enums.State.*;

@Repository
public class TicketDaoImpl extends BaseDaoImpl<Ticket> implements TicketDao {
    private static final String GET_ALL = "from ";
    private static final String GET_ALL_BY_USER_ID = "from Ticket where user.id = :user";
    private static final String GET_ALL_NEW_STATE = "from Ticket where user.role = 'EMPLOYEE' and state = 'NEW'";
    private static final String GET_ALL_BY_APPROVER_ID = "from Ticket where approver.id = :approver AND state in :state";
    private static final String GET_ALL_BY_ASSIGNEE_ID = "from Ticket where assignee.id = :assignee AND state in :state";
    private static final String GET_ALL_APPROVED_STATE = "from Ticket where state = 'APPROVED'";
    private static final String USER_COLUMN = "user";
    private static final String STATE_COLUMN = "state";
    private static final String ASSIGNEE_COLUMN = "assignee";
    private static final String APPROVER_COLUMN = "approver";

    private Logger logger = Logger.getLogger(this.getClass());

    public TicketDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
     }

    public List<Ticket> getAllForTickets() {
        logger.debug("Trying to find all Tickets");
        List<Ticket> tickets = getSession().createQuery((GET_ALL + (Ticket.class)
                 .getName()), Ticket.class).list();
        logger.info(String.format("Tickets were found  %s", tickets));
        return tickets;
    }

    public List<Ticket> getAllByUserId(Long userId) {
        logger.debug(String.format("Trying to find all Tickets for User with id = %s", userId));
        List<Ticket> tickets =  getSession().createQuery(GET_ALL_BY_USER_ID, Ticket.class)
            .setParameter(USER_COLUMN, userId).getResultList();
        logger.info(String.format("Tickets were found  %s", tickets));
        return tickets;
    }

    public List<Ticket> getAllWithStateNew() {
        logger.debug("Trying to find all Tickets with state NEW and role EMPLOYEE");
        List<Ticket> tickets = getAllByRole(GET_ALL_NEW_STATE);
        logger.info(String.format("Tickets were found  %s", tickets));
        return tickets;
    }

    public List<Ticket> getAllWithStateApproved() {
        logger.debug("Trying to find all Tickets with state APPROVED");
        List<Ticket> tickets = getAllByRole(GET_ALL_APPROVED_STATE);
        logger.info(String.format("Tickets were found  %s", tickets));
        return tickets;
    }

    public List<Ticket> getAllByApproverId(Long approverId) {
        logger.debug(String.format("Trying to find all Tickets by approver id = %s", approverId));
        List<State> states = Arrays.asList(APPROVED, CANCELED, DONE, DECLINED);
        List<Ticket> tickets = getAllByRoleAndState(approverId, states, GET_ALL_BY_APPROVER_ID, APPROVER_COLUMN);
        logger.info(String.format("Tickets were found  %s", tickets));
        return tickets;
    }

    public List<Ticket> getAllByAssigneeId(Long assigneeId) {
        logger.debug(String.format("Trying to find all Tickets by assignee id = %s", assigneeId));
        List<State> states = Arrays.asList(IN_PROGRESS,  DONE);
        List<Ticket> tickets = getAllByRoleAndState(assigneeId, states, GET_ALL_BY_ASSIGNEE_ID, ASSIGNEE_COLUMN);
        logger.info(String.format("Tickets were found  %s", tickets));
        return tickets;
    }

    private List<Ticket> getAllByRoleAndState(Long userId, List<State> states, String query, String roleColumn) {
        return getSession()
            .createQuery(query, Ticket.class)
            .setParameter(roleColumn, userId)
            .setParameterList(STATE_COLUMN, states).getResultList();
    }

    private List<Ticket> getAllByRole(String query) {
        return getSession().createQuery(query, Ticket.class).getResultList();
    }
}
