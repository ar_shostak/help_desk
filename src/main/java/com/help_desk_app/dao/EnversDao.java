package com.help_desk_app.dao;

import com.help_desk_app.history.Revision;
import java.util.List;

public interface EnversDao<T> {
    List<Revision<T>> getRevisionsById(long id);
}

