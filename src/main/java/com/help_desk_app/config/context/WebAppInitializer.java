package com.help_desk_app.config.context;

import com.help_desk_app.config.hibernate.HibernateUtils;
import com.help_desk_app.config.security.EncoderConfig;
import com.help_desk_app.config.security.WebSecurityConfig;
import com.help_desk_app.config.web.WebConfig;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration;

public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    private static final String SERVLET_MAPPING = "/";
    private static final String LOCATION = "D:/";
    private static final long MAX_FILE_SIZE = 1024 * 1024 * 5;//5MB
    private static final long MAX_REQUEST_SIZE = 1024 * 1024 * 10;//10MB
    private static final int FILE_SIZE_THRESHOLD = 0;

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[] {HibernateUtils.class, WebConfig.class,
                 EncoderConfig.class, WebSecurityConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return null;
    }

    @NotNull
    @Override
    protected String[] getServletMappings() {
        return new String[]{SERVLET_MAPPING};
    }

    @Override
    public void customizeRegistration(ServletRegistration.Dynamic registration) {
        registration.setInitParameter("throwExceptionIfNoHandlerFound", "true");
        registration.setMultipartConfig(new MultipartConfigElement(LOCATION, MAX_FILE_SIZE, MAX_REQUEST_SIZE, FILE_SIZE_THRESHOLD));
    }
}

