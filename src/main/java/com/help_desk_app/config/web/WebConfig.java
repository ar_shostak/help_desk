package com.help_desk_app.config.web;

import com.help_desk_app.converter.StringToTicketConverter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.format.FormatterRegistry;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.*;

@Configuration
@EnableWebMvc
@EnableGlobalMethodSecurity(prePostEnabled=true)
@ComponentScan("com.help_desk_app")
public class WebConfig implements WebMvcConfigurer {
    private static final String MESSAGE_PROPERTIES = "classpath:messages";
    private static final String ENCODING = "UTF-8";
    private static final String PATH_MAPPING = "/**";
    private static final String ALLOWED_HEADERS = "*";
    private static final String ALLOWED_ORIGIN= "http://localhost:3000";
    private static final String[] ALLOWED_METHODS = new String[]{"GET", "POST", "PUT", "OPTIONS", "DELETE"};

    private final ApplicationContext context;

    public WebConfig(final ApplicationContext context) {
        this.context = context;
    }

     @Override
     public void addCorsMappings(CorsRegistry registry) {
          registry.addMapping(PATH_MAPPING)
                  .allowedMethods(ALLOWED_METHODS)
                  .allowedHeaders(ALLOWED_HEADERS)
                  .allowedOrigins(ALLOWED_ORIGIN);
     }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename(MESSAGE_PROPERTIES);
        messageSource.setDefaultEncoding(ENCODING);
        return messageSource;
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToTicketConverter());
    }

    @Bean
    public StandardServletMultipartResolver multipartResolver(){
        return new StandardServletMultipartResolver();
    }
}

