package com.help_desk_app.exception;

public class FileStorageException  extends RuntimeException {
    public FileStorageException(String exceptionText) {super(exceptionText);}
}

