package com.help_desk_app.exception;

public class JsonConvertationException extends RuntimeException {
    public  JsonConvertationException(Object obj, Class clazz) {
        super(String.format("Could not convert %s to %s class", obj, clazz));
    }
}
