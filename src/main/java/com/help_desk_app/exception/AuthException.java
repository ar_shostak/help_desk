package com.help_desk_app.exception;

public class AuthException extends  RuntimeException {
    public  AuthException(String exceptionText) {super(exceptionText);}
}
