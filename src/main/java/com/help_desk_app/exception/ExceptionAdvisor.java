package com.help_desk_app.exception;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import javax.validation.ConstraintViolationException;
import java.net.URISyntaxException;

@RestControllerAdvice
public class ExceptionAdvisor extends ResponseEntityExceptionHandler {
    private Logger logger = Logger.getLogger(this.getClass());

    @ExceptionHandler({NotFoundException.class, UserNameNotFoundException.class, FilterException.class})
    public ResponseEntity<Object> handleNotFound(Exception ex) {
        logger.error(ex.getMessage());
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({URISyntaxException.class, AuthException.class, ConstraintViolationException.class,
            FileStorageException.class, IllegalArgumentException.class, JsonConvertationException.class})
    public ResponseEntity<Object> handleURISyntaxException(Exception ex) {
        logger.error(ex.getMessage());
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
