package com.help_desk_app.service;

import com.help_desk_app.entity.enums.Category;
import com.help_desk_app.entity.enums.Rate;
import com.help_desk_app.entity.enums.Urgency;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
public class CommonService {
    private static final  String CATEGORIES = "categories";
    private static final  String URGENCY = "urgency";
    private static final  String RATE = "rate";

    private Logger logger = Logger.getLogger(this.getClass());

    public Map<String, Object> getFixedVales(){
        Map<String, Object> map = new HashMap<>() ;
        map.put(CATEGORIES, Category.values());
        map.put(URGENCY, Urgency.values());
        map.put(RATE, Rate.getValues());
        logger.debug(String.format("Enums map was created : %s", map.entrySet()));
        return  map;
    }
}
