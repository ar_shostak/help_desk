package com.help_desk_app.service;

import com.help_desk_app.dao.AttachmentDao;
import com.help_desk_app.entity.Attachment;
import com.help_desk_app.exception.FileStorageException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class AttachmentService {
    private final static String STORAGE_EXCEPTION = "{service.AttachmentService.message}";
    private final static String EMPTY = "";

    private Logger logger = Logger.getLogger(this.getClass());

    private final AttachmentDao attachmentDao;

    public AttachmentService(AttachmentDao attachmentDao) {
        this.attachmentDao = attachmentDao;
    }

    public Set<Attachment> create(MultipartFile[] files) {
       logger.debug(String.format("Creating attachments from %s", files == null ? EMPTY : files ));
        return Arrays.stream(files)
                .peek(file -> logger.debug(String.format("Creating attachment from : %s", file)))
                .map(file -> {
                    try {
                        return attachmentDao.create(new Attachment(file.getBytes(),
                                file.getOriginalFilename(), file.getContentType()));
                    } catch (IOException e) {
                        logger.error(String.format("Exception when creating attachment from : %s", file), e);
                        throw new FileStorageException(STORAGE_EXCEPTION);
                    }
                })
                .peek(attachment -> logger.info(String.format("Attachment was created : %s", attachment)))
                .collect(Collectors.toSet());
    }
}

                                                                            