package com.help_desk_app.service;

import com.help_desk_app.converter.TicketConverter;
import com.help_desk_app.dao.TicketDao;
import com.help_desk_app.dto.ActionDto;
import com.help_desk_app.dto.TicketDto;
import com.help_desk_app.entity.Comment;
import com.help_desk_app.entity.Ticket;
import com.help_desk_app.entity.User;
import com.help_desk_app.entity.enums.Role;
import com.help_desk_app.entity.enums.State;
import com.help_desk_app.exception.NotFoundException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional(readOnly = true)
public class TicketService {
    private Logger logger = Logger.getLogger(this.getClass());

    private final TicketDao ticketDao;
    private final TicketConverter ticketConverter;
    private final AttachmentService attachmentService;
    private final AuthenticationService authenticationService;

    public TicketService(TicketDao ticketDao, TicketConverter ticketConverter,
                         AttachmentService attachmentService, AuthenticationService authenticationService) {
        this.ticketDao = ticketDao;
        this.ticketConverter = ticketConverter;
        this.attachmentService = attachmentService;
        this.authenticationService = authenticationService;
    }

    public Set<TicketDto> getAll() {
        logger.debug("Trying to get all tickets");
        return ticketDao.getAllForTickets()
                .stream()
                .map(ticketConverter::toDtoForTickets)
                .collect(Collectors.toSet());
    }

    public Ticket getTicketById(Long id) {
        logger.debug(String.format("Trying to get Ticket with id %s", id));
        return ticketDao.getOne(id).orElseThrow(() -> new NotFoundException(Ticket.class, id));
    }

    public TicketDto getOneForOverview(Long id) {
        return ticketConverter.toDto(getTicketById(id));
    }

    @Transactional
    public TicketDto create(TicketDto dto, MultipartFile[] files) {
        logger.debug(String.format("Creating Ticket from TicketDto %s with Attachments %s", dto, Arrays.toString(files)));
        dto.setCreatedOn(new Date());
        Ticket newTicket = ticketConverter.fromDto(dto);
        newTicket.setUser(authenticationService.getAuthenticateUser());
        newTicket.setAttachments(attachmentService.create(files));
        Ticket result = ticketDao.create(newTicket);
        logger.info(String.format("Ticket was created : %s", result));
        return ticketConverter.toDto(result);
    }

    @Transactional
    public TicketDto update(TicketDto dto, MultipartFile[] files) {
        logger.debug(String.format("Trying to update Ticket with TicketDto %s with Attachments %s", dto, Arrays.toString(files)));
        Ticket ticket = getTicketById(dto.getId());
        ticketConverter.copyNonNullProperties(dto, ticket);
        if(dto.getComment() != null) {
            ticket.getCommentSet().add(new Comment(authenticationService.getAuthenticateUser(),
                    dto.getComment(), new Date(), ticket));
        }
        ticket.setAttachments(attachmentService.create(files));
        ticketDao.update(ticket);
        logger.info(String.format("Ticket was updated : %s", ticket));
        return ticketConverter.toDto(ticket);
    }

    @Transactional
    public TicketDto updateState(ActionDto dto) {
        logger.debug(String.format("Trying to change state of Ticket with id %s", dto.getId()));
        State state = State.fromAction(dto.getAction()).orElseThrow(() -> new NotFoundException(State.class, dto.getId()));
        Ticket ticket = getTicketById(dto.getId());
        ticket.setState(state);
        setApproverOrAssignee(state, ticket);
        ticketDao.update(ticket);
        logger.info(String.format("Ticket state was changed to %s", ticket.getState()));
        return ticketConverter.toDtoForTickets(ticket);
    }

    public Set<TicketDto> getAllTicketsByUserId(Long id) {
        Role currentRole = authenticationService.getAuthenticateUser().getRole();
        switch (currentRole) {
            case MANAGER:
                return getAllTicketsForAdmin(id);
            case ENGINEER:
                return getAllTicketsForEngineer(id);
            default:
                return getAllTicketsForUser(id);
        }
    }

    private Set<TicketDto> getAllTicketsForUser(Long id) {
        logger.debug(String.format("Trying to get Tickets for User with id %s", id));
        return ticketDao.getAllByUserId(id)
                .stream()
                .map(ticketConverter::toDtoForTickets)
                .collect(Collectors.toSet());
    }

    private Set<TicketDto> getAllTicketsForAdmin(Long id) {
        logger.debug(String.format("Trying to get Tickets for Admin with id %s", id));
        return Stream.concat(Stream.concat(ticketDao.getAllByUserId(id).stream(),
                ticketDao.getAllWithStateNew().stream()),
                ticketDao.getAllByApproverId(id).stream())
                .map(ticketConverter::toDtoForTickets).collect(Collectors.toSet());
    }

    private Set<TicketDto> getAllTicketsForEngineer(Long id) {
        logger.debug(String.format("Trying to get Tickets for Engineer with id %s", id));
        return Stream.concat(ticketDao.getAllWithStateApproved().stream(),
                ticketDao.getAllByAssigneeId(id).stream())
                .map(ticketConverter::toDtoForTickets).collect(Collectors.toSet());
    }

    private void setApproverOrAssignee(State state, Ticket ticket) {
        logger.debug(String.format("Trying to set Approver or Assignee for Ticket %s", ticket));
        User user = authenticationService.getAuthenticateUser();
        if (state == State.APPROVED || state == State.DECLINED) {
            ticket.setApprover(user);
            logger.info(String.format("%s was set as an Approver for Ticket %s", user.getId(), ticket.getId()));
        }
        if (state == State.IN_PROGRESS || state == State.DONE) {
            ticket.setAssignee(user);
            logger.info(String.format("%s was set as an Assignee for Ticket %s", user.getId(), ticket.getId()));
        }
        if (state == State.IN_PROGRESS && user.getRole().equals(Role.MANAGER)) {
            ticket.setApprover(user);
            logger.info(String.format("%s was set as an Approver for Ticket %s", user.getId(), ticket.getId()));
        } else if (state == State.IN_PROGRESS && user.getRole().equals(Role.ENGINEER)) {
            ticket.setAssignee(user);
            logger.info(String.format("%s was set as an Assignee for Ticket %s", user.getId(), ticket.getId()));
        }
    }
}
