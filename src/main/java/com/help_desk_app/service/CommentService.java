package com.help_desk_app.service;

import com.help_desk_app.converter.CommentConverter;
import com.help_desk_app.dao.CommentDao;
import com.help_desk_app.dto.CommentDto;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class CommentService {
    private Logger logger = Logger.getLogger(this.getClass());

    private final CommentDao commentDao;
    private final CommentConverter commentConverter;

    public CommentService(CommentDao commentDao, CommentConverter commentConverter) {
        this.commentDao = commentDao;
        this.commentConverter = commentConverter;
    }

    public List<CommentDto> getByTicketId(Long id) {
        logger.debug(String.format("Trying to get Comments for Ticket with id %s", id));
        return commentDao.findByTicketId(id).stream()
                .map(commentConverter::toDto)
                .collect(Collectors.toList());
    }
}


