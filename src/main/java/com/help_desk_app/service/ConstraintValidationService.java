package com.help_desk_app.service;

import org.springframework.stereotype.Service;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

@Service
public class ConstraintValidationService {
    private static Validator validator;

    ConstraintValidationService() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    public static void validate(Object input) {
        Set<ConstraintViolation<Object>> violations = validator.validate(input);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }
}

