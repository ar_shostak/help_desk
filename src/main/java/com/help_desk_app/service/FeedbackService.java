package com.help_desk_app.service;

import com.help_desk_app.converter.FeedbackConverter;
import com.help_desk_app.dao.FeedbackDao;
import com.help_desk_app.dto.FeedbackDto;
import com.help_desk_app.entity.Feedback;
import com.help_desk_app.exception.NotFoundException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FeedbackService {
    private final FeedbackDao feedbackDao;
    private final FeedbackConverter  feedbackConverter;

    private Logger logger = Logger.getLogger(this.getClass());

    public FeedbackService(FeedbackDao  feedbackDao, FeedbackConverter  feedbackConverter) {
        this.feedbackDao =  feedbackDao;
        this.feedbackConverter =  feedbackConverter;
    }

    @Transactional(readOnly = true)
    public FeedbackDto findOneByTicketId(Long id) {
       logger.debug(String.format("Try to get Feedback for Ticket with id %s", id));
       Feedback feedback = feedbackDao.findOneByTicketId(id).orElseThrow(() -> new NotFoundException(Feedback.class, id));
       return feedbackConverter.toDto(feedback);
    }

    public FeedbackDto create(FeedbackDto feedbackDto) {
        logger.debug(String.format("Try to create Feedback from dto %s", feedbackDto));
        Feedback feedback = feedbackDao.create(feedbackConverter.fromDto(feedbackDto));
        FeedbackDto result = feedbackConverter.toDto(feedback);
        logger.info(String.format("Feedback was created : %s", result));
        return result;
    }

}
