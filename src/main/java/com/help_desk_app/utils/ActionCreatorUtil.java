package com.help_desk_app.utils;

import com.help_desk_app.entity.Ticket;
import com.help_desk_app.entity.User;
import com.help_desk_app.entity.enums.State;
import org.apache.log4j.Logger;

import java.util.*;

public class ActionCreatorUtil {
    private static final String SUBMIT = "Submit";
    private static final String CANCEL = "Cancel";
    private static final String LEAVE_FEEDBACK = "Leave Feedback";
    private static final String VIEW_FEEDBACK = "View Feedback";
    private static final String APPROVE = "Approve";
    private static final String DECLINE = "Decline";
    private static final String ASSIGNE = "Assign to me";
    private static final String DONE= "Done";

    private static Logger logger = Logger.getLogger(ActionCreatorUtil.class);

    public static Set<String> getActions(User user, Ticket ticket) {
        switch (user.getRole()) {
            case EMPLOYEE:
                return getEmployeeActions(user, ticket);
            case MANAGER:
                return getManagerActions(user, ticket);
            default:
                return getEngineerActions(ticket);
        }
    }

    private static Set<String> getEmployeeActions(User user, Ticket ticket) {
        logger.debug(String.format("Trying to create actions set for Ticket with id%s for User with id%s",
                ticket.getId(), user.getId()));
        Set<String> actions = new HashSet<>();
        if (ticket.getUser().getId().equals(user.getId())) {
            switch (ticket.getState()) {
                case DRAFT:
                case DECLINED:
                    actions.addAll(Arrays.asList(SUBMIT, CANCEL));
                    break;
                case DONE:
                    if (ticket.getFeedback() == null) {
                        actions.add(LEAVE_FEEDBACK);
                    }
            }
        }
        if (ticket.getFeedback() != null) {
            actions.add(VIEW_FEEDBACK);
        }
        return actions;
    }

    private static Set<String> getManagerActions(User user, Ticket ticket) {
        logger.debug(String.format("Trying to create actions set for Ticket with id%s for User with id%s",
                ticket.getId(), user.getId()));
        Set<String> actions = new HashSet<>();
        switch (ticket.getState()) {
            case DRAFT:
            case DECLINED:
                if (ticket.getUser().getId().equals(user.getId())) {
                    actions.addAll(Arrays.asList(SUBMIT, CANCEL));
                }
                break;
            case NEW:
                actions.addAll(Arrays.asList(APPROVE, DECLINE, CANCEL));
                break;
            case DONE:
                if (ticket.getUser().getId().equals(user.getId())) {
                    if (ticket.getFeedback() == null) {
                        actions.add(LEAVE_FEEDBACK);
                    }
                    actions.add(VIEW_FEEDBACK);
                }
        }
        return actions;
    }

    private static Set<String> getEngineerActions(Ticket ticket) {
        logger.debug(String.format("Trying to create actions set for Ticket with id%s for User with role ENGENEER",
                ticket.getId()));
        Set<String> actions = new HashSet<>();
        switch (ticket.getState()) {
            case APPROVED:
                actions.addAll(Arrays.asList(ASSIGNE, CANCEL));
                break;
            case IN_PROGRESS:
                actions.add(DONE);
                break;
            case DONE:
                if (ticket.getState().equals(State.DONE))
                    actions.add(VIEW_FEEDBACK);
        }
        return actions;
    }
}
