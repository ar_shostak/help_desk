package com.help_desk_app.utils;

import io.jsonwebtoken.*;
import org.apache.log4j.Logger;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import java.io.Serializable;
import java.util.Base64;
import java.util.Date;
import java.util.function.Function;

@Service
public class JwtUtils implements Serializable {
    public static final String JWT_SECRET = "epamjavamogilevepamjavamogilevepamjavamogilevepamjavamogilevepamjavamogilev";

    private Logger logger = Logger.getLogger(this.getClass());

    private String secretKey;

    public JwtUtils() {
        secretKey = Base64.getEncoder().encodeToString(JWT_SECRET.getBytes());
    }

    public String extractUsername(String token) {
        logger.debug("Trying to extract user name from token");
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        logger.debug("Trying to extract date from token");
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
    }

    private boolean isTokenExpired(String token) {
        logger.debug(String.format("Validating if token not expired. Token : %s", token));
        return extractExpiration(token).before(new Date());
    }

    public String createToken(String subject) {
        logger.debug(String.format("Trying to create token with subject %s", subject));
        Claims claims = Jwts.claims().setSubject(subject);
        String token = Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + (1000 * 60 * 60 * 5)))
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .compact();
        logger.info(String.format("Token was created : %s", token));
        return token;
    }

    public boolean validateToken(String token, UserDetails userDetails) {
        logger.debug(String.format("Trying to validate user with token : %s", token));
        String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
}

