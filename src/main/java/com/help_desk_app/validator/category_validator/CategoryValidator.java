package com.help_desk_app.validator.category_validator;

import com.help_desk_app.entity.enums.Category;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class CategoryValidator implements ConstraintValidator<ValidCategory, Category> {
    private List<Category> categories;

    @Override
    public void initialize(ValidCategory validCategory) {
        categories = Arrays.asList(Category.values());
    }

    @Override
    public boolean isValid(Category category,
                           ConstraintValidatorContext cxt) {
        return category != null && categories.contains(category);
    }
}
