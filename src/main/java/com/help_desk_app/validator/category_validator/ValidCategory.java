package com.help_desk_app.validator.category_validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = CategoryValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidCategory {
    String message() default "{validator.category_validator.ValidCategory.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
