package com.help_desk_app.validator.state_validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = StateValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidState {
    String message() default "{validator.state_validator.ValidState.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
