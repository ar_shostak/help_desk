package com.help_desk_app.validator.state_validator;

import com.help_desk_app.entity.enums.State;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class StateValidator implements ConstraintValidator<ValidState, State> {
    List<State> states;

    @Override
    public void initialize(ValidState validState) {
        states = Arrays.asList(State.values());
    }

    @Override
    public boolean isValid(State state,
                           ConstraintValidatorContext cxt) {
        return state != null && (states).contains(state);
    }
}
