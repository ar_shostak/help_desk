package com.help_desk_app.validator.urgency_validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UrgencyValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidUrgency {
    String message() default "{validator.urgency_validator.ValidUrgency.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

