package com.help_desk_app.validator.urgency_validator;

import com.help_desk_app.entity.enums.Urgency;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class UrgencyValidator implements ConstraintValidator<ValidUrgency, Urgency> {
    private List<Urgency> urgencies;

    @Override
    public void initialize(ValidUrgency validUrgency) {
        urgencies = Arrays.asList(Urgency.values());
    }

    @Override
    public boolean isValid(Urgency urgency,
                           ConstraintValidatorContext cxt) {
        return urgency != null && urgencies.contains(urgency);
    }
}


