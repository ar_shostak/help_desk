package com.help_desk_app.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.help_desk_app.entity.Attachment;
import com.help_desk_app.entity.enums.Category;
import com.help_desk_app.entity.enums.State;
import com.help_desk_app.entity.enums.Urgency;
import com.help_desk_app.validator.category_validator.ValidCategory;
import com.help_desk_app.validator.state_validator.ValidState;
import com.help_desk_app.validator.urgency_validator.ValidUrgency;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

public class TicketDto {

    private final static String NAME_PATTERN = "^[a-z0-9~.\"(),:;<>@\\[\\]!#$%&'*+-/=?^_`{|}]+$";
    private final static String DESCRIPTION_COMMENTS_PATTERN = "^[a-zA-Z0-9~.\"(),:;<>@\\[\\]!#$%&'*+\\-/=?^_`{|}]+$";
    private final static String INVALID_COMMENTS_DESCRIPTION = "{dto.TicketDto.comment_description.message}";
    private final static String INVALID_NAME = "{dto.TicketDto.name.message}";
    private final static String INVALID_DATE = "{dto.TicketDto.date.message}";
    private final static String DATE_PATTERN = "dd/MM/yyyy";
    private static final String REQUIRED = "{dto.TicketDto.required.message}";

    private Long id;

    @NotNull(message = REQUIRED)
    @Size(min  = 1, max = 100, message = INVALID_NAME)
    @Pattern(regexp = NAME_PATTERN, message = INVALID_NAME)
    private String ticketName;

    @Size(max = 500, message = INVALID_COMMENTS_DESCRIPTION)
    @Pattern(regexp = DESCRIPTION_COMMENTS_PATTERN, message = INVALID_COMMENTS_DESCRIPTION)
    private String description;

    @Size(max = 500, message = INVALID_COMMENTS_DESCRIPTION)
    @Pattern(regexp = DESCRIPTION_COMMENTS_PATTERN, message = INVALID_COMMENTS_DESCRIPTION)
    private String comment;

    @Future
    @JsonFormat(pattern = DATE_PATTERN)
    private Date createdOn;

    @Future(message = INVALID_DATE)
    @JsonFormat(pattern = DATE_PATTERN )
    private Date desiredResolutionDate;
    private String assigneeName;
    private String approverName;
    private UserDto user;

    @NotNull(message = REQUIRED)
    @ValidState
    private State state;

    @NotNull
    @ValidCategory
    private Category category;

    @NotNull(message = REQUIRED)
    @ValidUrgency
    private Urgency urgency;

    private Set<Attachment> attachments;
    private Set<String> actions;

    public TicketDto(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTicketName() {
        return ticketName;
    }

    public void setTicketName(String ticketName) {
        this.ticketName = ticketName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getDesiredResolutionDate() {
        return desiredResolutionDate;
    }

    public void setDesiredResolutionDate(Date desiredResolutionDate) {
        this.desiredResolutionDate = desiredResolutionDate;
    }

    public String getAssigneeName() {
        return assigneeName;
    }

    public void setAssigneeName(String assigneeName) {
        this.assigneeName = assigneeName;
    }

    public String getApproverName() {
        return approverName;
    }

    public void setApproverName(String approverName) {
        this.approverName = approverName;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Urgency getUrgency() {
        return urgency;
    }

    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }

    public Set<String> getActions() {
        return actions;
    }

    public void setActions(Set<String> actions) {
        this.actions = actions;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Set<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(Set<Attachment> attachments) {
        this.attachments = attachments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TicketDto that = (TicketDto) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(ticketName, that.ticketName) &&
                Objects.equals(description, that.description) &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(desiredResolutionDate, that.desiredResolutionDate) &&
                Objects.equals(assigneeName, that.assigneeName) &&
                Objects.equals(approverName, that.approverName) &&
                Objects.equals(user, that.user) &&
                state == that.state &&
                category == that.category &&
                urgency == that.urgency;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ticketName, description, createdOn, desiredResolutionDate, assigneeName, approverName, user, state, attachments, category, urgency, comment);
    }

    @Override
    public String toString() {
        return "TicketDto{" +
                "id=" + id +
                ", ticketName='" + ticketName + '\'' +
                ", description='" + description + '\'' +
                ", createdOn=" + createdOn +
                ", desiredResolutionDate=" + desiredResolutionDate +
                ", assigneeName='" + assigneeName + '\'' +
                ", approverName='" + approverName + '\'' +
                ", userName=" + user +
                ", state=" + state +
                ", category=" + category +
                ", comment=" + comment +
                ", urgency=" + urgency +
                //", attachments=" + attachments +
                '}';
    }
}
