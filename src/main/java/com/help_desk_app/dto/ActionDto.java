package com.help_desk_app.dto;

import java.util.Objects;

public class ActionDto {
    private Long id;
    private String action;

    public ActionDto(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActionDto actionDto = (ActionDto) o;
        return Objects.equals(id, actionDto.id) &&
                Objects.equals(action, actionDto.action);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, action);
    }

    @Override
    public String toString() {
        return "ActionDto{" +
                "id=" + id +
                ", action='" + action + '\'' +
                '}';
    }
}
