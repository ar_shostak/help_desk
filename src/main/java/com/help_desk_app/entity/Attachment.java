package com.help_desk_app.entity;

import org.hibernate.envers.Audited;
import javax.persistence.*;
import java.util.Objects;

@Entity
@Audited
@Table(name="Attachment")
public class Attachment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    private byte[] blob;

    private String contentType;

    private String name;

    public Attachment(){ }

    public Attachment(byte[] blob, String name, String contentType) {
        this.blob = blob;
        this.name = name;
        this.contentType = contentType;
    }

    public Long getId() {
        return id;
    }

    public byte[] getBlob() {
        return blob;
    }

    public void setBlob(byte[] blob) {
        this.blob = blob;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Attachment that = (Attachment) o;
        return  Objects.equals(contentType, that.contentType) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, contentType);
    }

    @Override
    public String toString() {
        return "Attachment{" +
                "id=" + id +
                ", file size =" + blob.length +
                ", contentType =" + contentType +
                ", name='" + name + '\'' +
                '}';
    }
}
