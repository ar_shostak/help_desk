package com.help_desk_app.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.help_desk_app.entity.enums.Category;
import com.help_desk_app.entity.enums.State;
import com.help_desk_app.entity.enums.Urgency;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import javax.persistence.*;
import java.util.*;

@Audited
@Entity
@Table(name="Ticket")
public class Ticket {
    private static final String PATTERN = "dd/MM/yyyy";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String ticketName;

    private String description;

    @JsonFormat(pattern = PATTERN)
    private Date createdOn;

    @JsonFormat(pattern = PATTERN)
    private Date desiredResolutionDate;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @Fetch(value = FetchMode.JOIN)
    @ManyToOne
    @JoinColumn(name = "assignee_id")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
            property = "id")
    private User assignee;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @Fetch(value = FetchMode.JOIN)
    @ManyToOne
    @JoinColumn(name = "approver_id")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
            property = "id")
    private User approver;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @Fetch(value = FetchMode.JOIN)
    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
            property = "id")
    private User user;

    @Enumerated(EnumType.STRING)
    private State state;

    @Enumerated(EnumType.STRING)
    private Category category;

    @Enumerated(EnumType.STRING)
    private Urgency urgency;

    @Fetch(value = FetchMode.JOIN)
    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE })
    @JoinTable(name = "ticket_attachment",
            joinColumns = @JoinColumn(name = "ticket_id"),
            inverseJoinColumns = @JoinColumn(name = "attachment_id"))
    private Set<Attachment> attachments = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "ticket_id")
    private Set<Comment> commentSet = new HashSet<>();

    @OneToOne(mappedBy = "ticket")
    private Feedback feedback;

    public Ticket() {
    }

    public Ticket(String ticketName, String description, Date desiredResolutionDate, Category category, Urgency urgency) {
        this.ticketName = ticketName;
        this.description = description;
        this.desiredResolutionDate = desiredResolutionDate;
        this.category = category;
        this.urgency = urgency;
    }

    public Long getId() {
        return id;
    }

    public String getTicketName() {
        return ticketName;
    }

    public String getDescription() {
        return description;
    }

    public Date getCreatedOn(){
        return createdOn;
    }

    public User getAssignee() {
        return assignee;
    }

    public User getUser() {
        return user;
    }

    public State getState() {
        return state;
    }

    public Category getCategory() {
        return category;
    }

    public Feedback getFeedback() {
        return feedback;
    }

    public Urgency getUrgency() {
        return urgency;
    }

    public Set<Attachment> getAttachments() {
        return attachments;
    }

    public User getApprover() {
        return approver;
    }

    public Set<Comment> getCommentSet() {
        return commentSet;
    }

    public Date getDesiredResolutionDate() {
        return desiredResolutionDate;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTicketName(String ticketName) {
        this.ticketName = ticketName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public void setDesiredResolutionDate(Date desiredResolutionDate) {
        this.desiredResolutionDate = desiredResolutionDate;
    }

    public void setAssignee(User assignee) {
        this.assignee = assignee;
    }

    public void setApprover(User approver) {
        this.approver = approver;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }

    public void setAttachments(Set<Attachment> attachments) {
        this.attachments = attachments;
    }

    public void setCommentSet(Set<Comment> commentSet) {
        this.commentSet = commentSet;
    }

    public void setFeedback(Feedback feedback) {
        this.feedback = feedback;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return Objects.equals(id, ticket.id) &&
                ticketName.equals(ticket.ticketName) &&
                description.equals(ticket.description) &&
                Objects.equals(createdOn, ticket.createdOn) &&
                desiredResolutionDate.equals(ticket.desiredResolutionDate) &&
                user.equals(ticket.user) &&
                state == ticket.state &&
                category == ticket.category &&
                urgency == ticket.urgency &&
                Objects.equals(feedback, ticket.feedback);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ticketName, description, createdOn, desiredResolutionDate, user, state, category, commentSet, urgency, feedback);
    }


    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", ticketName='" + ticketName + '\'' +
                ", description='" + description + '\'' +
                ", createdOn=" + createdOn +
                ", desiredResolutionDate=" + desiredResolutionDate +
                ", assignee=" + assignee +
                ", approver=" + approver +
                ", user=" + user +
                ", state=" + state +
                ", category=" + category +
                ", urgency=" + urgency +
                ", attachments=" + attachments +
                ", feedback=" + feedback +
                '}';
    }
}
