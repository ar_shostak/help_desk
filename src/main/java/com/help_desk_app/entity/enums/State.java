package com.help_desk_app.entity.enums;

import java.util.Optional;

public enum State {
    DRAFT("Create"),
    NEW("Submit"),
    APPROVED("Approve"),
    DECLINED("Decline"),
    IN_PROGRESS("Assign to me"),
    DONE("Done"),
    CANCELED("Cancel");

    private String action;

    State(String action) {
        this.action = action;
    }

    public static Optional<State> fromAction(String action) {
        Optional<State> newState = Optional.empty();
        for (State state : values()) {
            if (state.getAction().equals(action)) {
                newState = Optional.ofNullable(state);
            }
        }
        return newState;
    }

    public String getAction() {
        return action;
    }
}
