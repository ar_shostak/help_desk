package com.help_desk_app.entity.enums;

public enum ChangeType {
    CREATION,
    STATUS_CHANGE,
    CHANGE,
    ATTACHMENT_ADD,
    ATTACHMENT_REMOVE,
}
