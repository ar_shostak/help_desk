package com.help_desk_app.entity.enums;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public enum Rate {
    ONE(1),
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5);

    private Byte number;

    Rate(Integer number) {
        this.number = number.byteValue();
    }

    public static Optional<Rate> fromNumber(Byte userRate) {
        Optional<Rate> newState = Optional.empty();
        for (Rate number : values()) {
            if (number.getNumber().equals(userRate)) {
                newState = Optional.of(number);
            }
        }
        return newState;
    }

    public Byte getNumber() {
        return number;
    }

    public static Set<Byte> getValues() {
        return Arrays.stream(Rate.values()).map(Rate::getNumber).collect(Collectors.toSet());
    }
}

