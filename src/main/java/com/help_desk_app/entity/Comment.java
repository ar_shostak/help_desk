package com.help_desk_app.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

@Entity
@Audited
@Table(name="Comment")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Audited(targetAuditMode = NOT_AUDITED)
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    private String text;

    @JsonFormat(pattern = "dd/MM/yyyy" )
    private Date createdDate;

    @Audited(targetAuditMode = NOT_AUDITED)
    @OneToOne
    private Ticket ticket;

    public Comment(){}

    public Comment(User user, String text, Date createdDate, Ticket ticket) {
        this.user = user;
        this.text = text;
        this.createdDate = createdDate;
        this.ticket = ticket;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date date) {
        this.createdDate = date;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return Objects.equals(id, comment.id) &&
                Objects.equals(user, comment.user) &&
                Objects.equals(text, comment.text) &&
                Objects.equals(createdDate, comment.createdDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, text, createdDate);
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", user=" + user +
                ", text='" + text + '\'' +
                ", date=" + createdDate +
                '}';
    }
}
