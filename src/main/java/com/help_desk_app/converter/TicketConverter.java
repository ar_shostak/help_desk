package com.help_desk_app.converter;

import com.help_desk_app.config.security.CustomUserPrincipal;
import com.help_desk_app.dto.TicketDto;
import com.help_desk_app.entity.Comment;
import com.help_desk_app.entity.Ticket;
import com.help_desk_app.entity.User;
import com.help_desk_app.service.AuthenticationService;
import com.help_desk_app.utils.ActionCreatorUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import java.util.HashSet;
import java.util.Set;

@Component
public class TicketConverter {
    private static final String SPACE = "";
    private Logger logger = Logger.getLogger(this.getClass());

    private final AuthenticationService authenticationService;
    private final UserConverter userConverter;

    public TicketConverter(AuthenticationService authenticationService,
                           UserConverter userConverter) {
        this.authenticationService = authenticationService;
        this.userConverter = userConverter;
    }

    public TicketDto toDtoForTickets(Ticket ticket) {
        logger.debug(String.format("Creating TicketDto from %s", ticket));
        User user = ((CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        TicketDto dto = new TicketDto();
        dto.setId(ticket.getId());
        dto.setTicketName(ticket.getTicketName());
        dto.setDesiredResolutionDate(ticket.getDesiredResolutionDate());
        dto.setState(ticket.getState());
        dto.setUrgency(ticket.getUrgency());
        dto.setActions(ActionCreatorUtil.getActions(user, ticket));
        logger.info(String.format("TicketDto was created : %s", dto));
        return dto;
    }

    public TicketDto toDto(Ticket ticket) {
        logger.debug(String.format("Creating TicketDto from %s", ticket));
        TicketDto dto = new TicketDto();
        copyNonNullProperties(ticket, dto);
        if (ticket.getUser() != null) {
            dto.setUser(userConverter.toDto(ticket.getUser()));
        }
        if (ticket.getApprover() != null) {
            dto.setApproverName(ticket.getApprover().getFirstName() + SPACE + ticket.getApprover().getLastName());
        }
        if (ticket.getAssignee() != null) {
            dto.setAssigneeName(ticket.getAssignee().getFirstName() + SPACE + ticket.getAssignee().getLastName());
        }
        logger.info(String.format("TicketDto was created : %s", dto));
        return dto;
    }

    public Ticket fromDto(TicketDto dto) {
        logger.debug(String.format("Creating TicketDto from %s", dto));
        Ticket ticket = new Ticket();
        copyNonNullProperties(dto, ticket);
        if(dto.getComment() != null) {
            Comment comment = new Comment(authenticationService.getAuthenticateUser(), dto.getComment(), dto.getCreatedOn(), ticket);
            ticket.getCommentSet().add(comment);
        }
        logger.info(String.format("Ticket was created : %s", ticket));
        return ticket;
    }

    public void copyNonNullProperties(Object source, Object destination){
        BeanUtils.copyProperties(source, destination,
                getNullPropertyNames(source));
    }

    private String[] getNullPropertyNames (Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<Object> emptyNames = new HashSet<>();
        for(java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) emptyNames.add(pd.getName());
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }
}
