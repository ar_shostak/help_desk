package com.help_desk_app.converter;

import com.help_desk_app.dto.UserDto;
import com.help_desk_app.entity.User;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class UserConverter {
    private Logger logger = Logger.getLogger(this.getClass());

    public UserDto toDto(final User entity) {
        logger.debug(String.format("Creating UserDto from %s", entity));
        UserDto dto = new UserDto(entity.getId(), entity.getFirstName(), entity.getLastName(),
                entity.getRole(), entity.getEmail());
        logger.info(String.format("UserDto was created : %s", dto));
        return dto;
    }
}
