package com.help_desk_app.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.help_desk_app.dto.TicketDto;
import com.help_desk_app.service.ConstraintValidationService;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.convert.converter.Converter;
import java.io.IOException;

public class StringToTicketConverter implements Converter<String, TicketDto> {
    private Logger logger = Logger.getLogger(this.getClass());

        @Override
        public TicketDto convert(@NotNull String from) {
            logger.debug(String.format("Creating TicketDto from String %s", from));
            TicketDto dto = null;
            try {
                dto = new ObjectMapper().readValue(from, TicketDto.class);
            } catch (IOException e) {
                logger.error(String.format("Exception when creating TicketDto from : %s", dto));
                logger.error(e.getStackTrace());
            }
            ConstraintValidationService.validate(dto);
            logger.info(String.format("TicketDto was created : %s", dto));
            return dto;
        }

}
