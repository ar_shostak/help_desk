package com.help_desk_app.converter;

import com.help_desk_app.dto.CommentDto;
import com.help_desk_app.entity.Comment;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class CommentConverter {
    private Logger logger = Logger.getLogger(this.getClass());

    public CommentDto toDto(Comment comment) {
        logger.debug(String.format("Creating CommentDto from %s", comment));
        CommentDto dto = new CommentDto();
        dto.setId(comment.getId());
        dto.setUser(comment.getUser().getFirstName() + " " + comment.getUser().getLastName());
        dto.setText(comment.getText());
        dto.setDate(comment.getCreatedDate());
        logger.info(String.format("CommentDto was created : %s", dto));
        return dto;
    }
}
