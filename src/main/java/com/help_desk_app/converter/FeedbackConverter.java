package com.help_desk_app.converter;

import com.help_desk_app.dto.FeedbackDto;
import com.help_desk_app.entity.Feedback;
import com.help_desk_app.entity.User;
import com.help_desk_app.entity.enums.Rate;
import com.help_desk_app.exception.NotFoundException;
import com.help_desk_app.service.AuthenticationService;
import com.help_desk_app.service.TicketService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class FeedbackConverter {
    private Logger logger = Logger.getLogger(this.getClass());

    private TicketService ticketService;
    private AuthenticationService authenticationService;

    public FeedbackConverter(TicketService ticketService, AuthenticationService authenticationService) {
        this.ticketService = ticketService;
        this.authenticationService = authenticationService;
    }

    public FeedbackDto toDto(Feedback feedback) {
        logger.debug(String.format("Creating FeedbackDto from %s", feedback));
        FeedbackDto dto = new FeedbackDto();
        dto.setRate(feedback.getRate().getNumber());
        dto.setText(feedback.getText());
        dto.setTicketId(feedback.getTicket().getId());
        logger.info(String.format("FeedbackDto was created : %s", dto));
        return dto;
    }

    public Feedback fromDto(FeedbackDto dto) {
        logger.debug(String.format("Creating Feedback from Dto : %s", dto));
        User user = authenticationService.getAuthenticateUser();
        Rate rate = Rate.fromNumber(dto.getRate()).orElseThrow(() -> new NotFoundException(Rate.class, dto.getRate()));
        Feedback feedback = new Feedback(user, rate, new Date(), dto.getText(), ticketService.getTicketById(dto.getTicketId()));
        logger.info(String.format("Feedback was created : %s", feedback));
        return feedback;
    }
}
