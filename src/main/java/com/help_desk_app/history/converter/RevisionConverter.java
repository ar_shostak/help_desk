package com.help_desk_app.history.converter;

import com.help_desk_app.entity.enums.ChangeType;
import com.help_desk_app.history.detector.*;
import com.help_desk_app.history.change.Change;
import com.help_desk_app.history.Revision;
import org.apache.log4j.Logger;
import org.hibernate.envers.RevisionType;
import org.springframework.stereotype.Component;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class RevisionConverter<Ticket> {
    private Logger logger = Logger.getLogger(this.getClass());
    private Set<ChangeDetector<Ticket>> changeDetectorMap;

    public RevisionConverter(Set<ChangeDetector<Ticket>> detectors) {
        this.changeDetectorMap = detectors;
    }

    public Optional<Change> prepareCreationChange(Revision<Ticket> revision) {
        return Optional.empty();
    }

    public List<Change> convert(Revision<Ticket> previous, Revision<Ticket> after) {
        List<Change> changes = new LinkedList<>();
        changeDetectorMap.forEach(changeDetector -> changes.addAll(changeDetector.detectChange(previous, after)));
        return changes;
    }

    public List<Change> convertRevisions(List<Revision<Ticket>> revisions) {
        logger.debug("Trying to convert Revisions to Changes");
        List<Change> changeList = new LinkedList<>();

        if (!revisions.isEmpty()) {
            revisions.stream()
                    .filter(revision -> RevisionType.ADD.equals(revision.getRevisionType()))
                    .findFirst().ifPresent(revision -> prepareCreationChange(revision)
                    .ifPresent(changeList::add));

            Iterator<Revision<Ticket>> iterator = revisions.iterator();
            Revision<Ticket> currentRevision = iterator.next();

            while (iterator.hasNext()) {
                Revision<Ticket> nextRevision = iterator.next();
                List<Change> changes = convert(currentRevision, nextRevision);
                changeList.addAll(changes);
                currentRevision = nextRevision;
            }

            changeList.addAll(revisions.stream()
                    .filter(revision -> RevisionType.ADD.equals(revision.getRevisionType()))
                    .peek(attachment -> logger.debug("Creating a Change for the new Ticket"))
                    .map(revision -> new Change(ChangeType.CREATION,
                            null, null,
                            revision.getRevisionEntity().getRevisionDate(),
                            revision.getRevisionEntity().getUserName()))
                    .peek(change -> logger.debug("Change for the new ticket was created"))
                    .collect(Collectors.toSet()));
        }
        if(changeList.size() == 0) {
            logger.info("Change list is empty");
        }
        return changeList;
    }
}

