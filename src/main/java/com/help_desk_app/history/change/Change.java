package com.help_desk_app.history.change;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.help_desk_app.entity.enums.ChangeType;
import java.util.Date;
import java.util.Objects;

public class Change {
    private static final String DATE_PATTERN = "dd/MM/yyyy HH:mm";

    private final ChangeType type;
    private final String originValue;
    private final String newValue;

    @JsonFormat(pattern = DATE_PATTERN)
    private final Date date;
    private final String userName;

    public Change(ChangeType type, String originValue, String newValue, Date date, String userName) {
        this.type = type;
        this.originValue = originValue;
        this.newValue = newValue;
        this.date = date;
        this.userName = userName;
    }

    public ChangeType getType() {
        return type;
    }

    public String getOriginValue() {
        return originValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public Date getDate() {
        return date;
    }

    public String getUserName() {
        return userName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Change change = (Change) o;
        return type == change.type &&
                Objects.equals(originValue, change.originValue) &&
                Objects.equals(newValue, change.newValue) &&
                Objects.equals(date, change.date) &&
                Objects.equals(userName, change.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, originValue, newValue, date, userName);
    }

    @Override
    public String toString() {
        return "Change{" +
                "type=" + type +
                ", originValue='" + originValue + '\'' +
                ", newValue='" + newValue + '\'' +
                ", date=" + date +
                ", userName='" + userName + '\'' +
                '}';
    }
}
