package com.help_desk_app.history;

import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "REVINFO")
@RevisionEntity(BaseRevisionListener.class)
public class BaseRevisionEntity extends DefaultRevisionEntity {

    @Column(name = "username")
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "BaseRevisionEntity{" +
                super.toString() + '\'' +
                "userName='" + userName + '\'' +
                '}';
    }
}

