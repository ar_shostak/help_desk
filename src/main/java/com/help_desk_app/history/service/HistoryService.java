package com.help_desk_app.history.service;

import com.help_desk_app.dao.EnversDao;
import com.help_desk_app.history.converter.RevisionConverter;
import com.help_desk_app.history.change.Change;
import com.help_desk_app.entity.Ticket;
import com.help_desk_app.history.Revision;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Comparator;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class HistoryService {
    private Logger logger = Logger.getLogger(this.getClass());

    private final EnversDao<Ticket> enversDao;
    private final RevisionConverter<Ticket> revisionConverter;

    public HistoryService(RevisionConverter<Ticket> revisionConverter, EnversDao<Ticket> enversDao) {
        this.enversDao = enversDao;
        this.revisionConverter = revisionConverter;
    }

    public List<Change> getHistoryByTicketId(Long id) {
        logger.debug(String.format("Trying to get Revisions for Ticket with id%s", id));
        List<Revision<Ticket>> revisions = enversDao.getRevisionsById(id);
        List<Change> result = revisionConverter.convertRevisions(revisions);
        result.sort(Comparator.comparing(Change::getDate).reversed());
     return result;
    }

}

