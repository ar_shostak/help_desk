package com.help_desk_app.history;

import org.hibernate.envers.RevisionType;
import java.util.Objects;

public class Revision<T> {
    private final T entity;
    private final BaseRevisionEntity revisionEntity;
    private final RevisionType revisionType;

    public Revision(final Object[] objects) {
        this.entity = (T) objects[0];
        this.revisionEntity = (BaseRevisionEntity) objects[1];
        this.revisionType = (RevisionType) objects[2];
    }

    public T getEntity() {
        return entity;
    }

    public BaseRevisionEntity getRevisionEntity() {
        return revisionEntity;
    }

    public RevisionType getRevisionType() {
        return revisionType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Revision<?> revision = (Revision<?>) o;
        return Objects.equals(entity, revision.entity) &&
                Objects.equals(revisionEntity, revision.revisionEntity) &&
                revisionType == revision.revisionType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(entity, revisionEntity, revisionType);
    }

    @Override
    public String toString() {
        return "Revision{" +
                "entity=" + entity +
                ", revisionEntity=" + revisionEntity +
                ", revisionType=" + revisionType +
                '}';
    }
}

