package com.help_desk_app.history;

import com.help_desk_app.config.security.CustomUserPrincipal;
import com.help_desk_app.entity.User;
import org.hibernate.envers.RevisionListener;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.core.context.SecurityContextHolder;

@Configurable
public class BaseRevisionListener implements RevisionListener {
    private static final String SPACE = " ";

    @Override
    public void newRevision(Object revisionEntity) {
        BaseRevisionEntity entity = (BaseRevisionEntity) revisionEntity;
        User user = ((CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal()).getUser();
        entity.setUserName(user.getFirstName() + SPACE + user.getLastName());
    }
}

