package com.help_desk_app.history.detector;

import com.help_desk_app.history.change.Change;
import com.help_desk_app.entity.Ticket;
import com.help_desk_app.entity.enums.ChangeType;
import com.help_desk_app.history.Revision;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

@Component
public class TicketChangeDetector implements ChangeDetector<Ticket> {
    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public List<Change> detectChange(Revision<Ticket> previous,
                                     Revision<Ticket> after) {
        logger.debug(String.format("Trying to create a change list for the Ticket with id%s", after.getEntity().getId()));
        List<Change> changes = new ArrayList<>();
        boolean stateIsChanged = stateIsChanged(previous.getEntity(), after.getEntity());
        if (stateIsChanged) {
            changes.add(createStateChange(previous, after));
        } else changes.add(createChange(after));
        logger.info(String.format("A Change list for the Ticket with id%s was created : %s",
                after.getEntity().getId(), changes));
        return changes;
    }

    private boolean stateIsChanged(Ticket prev, Ticket after) {
        return prev.getState() != null && !prev.getState().equals(after.getState());
    }

    private Change createStateChange(Revision<Ticket> previous, Revision<Ticket> after) {
        logger.debug(String.format("Creating a state Change for the Ticket with id%s", after.getEntity().getId()));
        return new Change(ChangeType.STATUS_CHANGE,
                previous.getEntity().getState().name(),
                after.getEntity().getState().name(),
                after.getRevisionEntity().getRevisionDate(),
                after.getRevisionEntity().getUserName());
    }

    private Change createChange(Revision<Ticket> after) {
        logger.debug(String.format("Creating a Change for the Ticket with id%s", after.getEntity().getId()));
        return new Change(ChangeType.CHANGE,
                null,
                null,
                after.getRevisionEntity().getRevisionDate(),
                after.getRevisionEntity().getUserName());
    }
}

