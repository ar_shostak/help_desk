package com.help_desk_app.history.detector;

import com.help_desk_app.history.change.Change;
import com.help_desk_app.history.Revision;
import java.util.List;

public interface ChangeDetector<T> {
    List<Change> detectChange(Revision<T> previousRev, Revision<T> afterRev);
}
