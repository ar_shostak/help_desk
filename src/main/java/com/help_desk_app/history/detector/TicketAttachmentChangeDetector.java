package com.help_desk_app.history.detector;

import com.help_desk_app.entity.Attachment;
import com.help_desk_app.entity.Ticket;
import com.help_desk_app.entity.enums.ChangeType;
import com.help_desk_app.history.Revision;
import com.help_desk_app.history.change.Change;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import java.util.*;

@Component
public class TicketAttachmentChangeDetector implements ChangeDetector<Ticket> {
    private static final String EMPTY = "";
    private static final String SPLITTER = "; ";

    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public List<Change> detectChange(Revision<Ticket> previous,
                                     Revision<Ticket> after) {
        logger.debug(String.format("Trying to create a change list for the Ticket with id%s", after.getEntity().getId()));
        boolean stateIsChanged = attachmentsIsChanged(previous.getEntity(), after.getEntity());
        return stateIsChanged ? createChange(previous, after) : new ArrayList<>();
    }

    private boolean attachmentsIsChanged(Ticket prev, Ticket after) {
        return !prev.getAttachments().equals(after.getAttachments());
    }

    private List<Change> createChange(Revision<Ticket> previous,
                                      Revision<Ticket> after) {
        List<Change> changes = new ArrayList<>();
        createRemoveChange(previous, after).ifPresent(changes::add);
        createAddChange(previous, after).ifPresent(changes::add);
        logger.info(String.format("A change list for the Ticket with id%s was created : %s",
                after.getEntity().getId(), changes));
        return changes;
    }

    private Optional<Change> createRemoveChange(Revision<Ticket> previous,
                                Revision<Ticket> after) {
        logger.debug(String.format("Trying to create an attachment removed change for the Ticket with id%s",
                after.getEntity().getId()));
        Set<Attachment> previousFiles = previous.getEntity().getAttachments();
        Set<Attachment> afterFiles  = after.getEntity().getAttachments();

        String deletedFiles = EMPTY;

        for (Attachment prev : previousFiles) {
            if (!(afterFiles.contains(prev))) {
                deletedFiles = deletedFiles.concat(prev.getName() + SPLITTER);
            }

        }
        if (deletedFiles.equals(EMPTY)) {
            logger.info("No attachment were removed");
            return Optional.empty();
        }
        Optional<Change> change = Optional.of(new Change(ChangeType.ATTACHMENT_REMOVE,
                deletedFiles,
                EMPTY,
                after.getRevisionEntity().getRevisionDate(),
                after.getRevisionEntity().getUserName()));
        logger.info(String.format("An attachment removed change was created : %s", change));
        return change;
    }

    private Optional<Change> createAddChange(Revision<Ticket> previous,
                                Revision<Ticket> after) {
        logger.debug(String.format("Trying to create an attachment added change for the Ticket with id%s",
                after.getEntity().getId()));
        Set<Attachment> previousFiles = previous.getEntity().getAttachments();
        Set<Attachment> afterFiles  = after.getEntity().getAttachments();

        String addedFiles = EMPTY;
        for(Attachment att : afterFiles) {
            if(!(previousFiles.contains(att))) {
                addedFiles = addedFiles.concat(att.getName() + SPLITTER);
            }
        }

        if (addedFiles.equals(EMPTY)) {
            return Optional.empty();
        }

        Optional<Change> change = Optional.of(new Change(ChangeType.ATTACHMENT_ADD,
                EMPTY,
                addedFiles,
                after.getRevisionEntity().getRevisionDate(),
                after.getRevisionEntity().getUserName()));
        logger.info(String.format("An attachment added change was created : %s", change));
        return change;
    }
}


