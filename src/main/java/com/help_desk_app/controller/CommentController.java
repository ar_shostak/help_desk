package com.help_desk_app.controller;

import com.help_desk_app.dto.CommentDto;
import com.help_desk_app.service.CommentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/comments")
public class CommentController {

    private final CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<List<CommentDto>> getComments(@PathVariable final Long id) {
        return ResponseEntity.ok(commentService.getByTicketId(id));
    }

}
