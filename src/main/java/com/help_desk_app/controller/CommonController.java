package com.help_desk_app.controller;

import com.help_desk_app.service.CommonService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

@RestController
@RequestMapping("/")
public class CommonController {

    private final CommonService commonService;

    public CommonController(CommonService commonService) {
        this.commonService = commonService;
    }

    @GetMapping(value = "/common")
    public ResponseEntity<Map<String, Object>> getComments() {
        return ResponseEntity.ok(commonService.getFixedVales());
    }
}
