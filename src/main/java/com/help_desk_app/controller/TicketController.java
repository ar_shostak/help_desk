package com.help_desk_app.controller;

import com.help_desk_app.dto.ActionDto;
import com.help_desk_app.dto.TicketDto;
import com.help_desk_app.service.TicketService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;

@RestController
@RequestMapping("/tickets")
public class TicketController {

    private final TicketService ticketService;

    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @GetMapping
    public ResponseEntity<Set<TicketDto>> get() {
            return ResponseEntity.ok(ticketService.getAll());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<TicketDto> get(@PathVariable final Long id) {
        return ResponseEntity.ok(ticketService.getOneForOverview(id));
    }

    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('MANAGER')")
    @PostMapping
    public ResponseEntity<TicketDto> create(@RequestParam(value = "ticket") final TicketDto dto,
                       @RequestParam(value = "attachments", required = false) final MultipartFile[] multipartFiles) {

        TicketDto created = ticketService.create(dto, multipartFiles);
        final URI uri =
                ServletUriComponentsBuilder.fromCurrentRequest()
                        .path("/{id}")
                        .buildAndExpand(created.getId())
                        .toUri();

        return ResponseEntity.created(uri).body(created);
    }

    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('MANAGER')")
    @PutMapping(value = "/{id}")
    public ResponseEntity<TicketDto> update(final HttpServletRequest request,
            @RequestParam(value = "ticket") final TicketDto dto,
            @RequestParam(value = "attachments", required = false) final MultipartFile[] multipartFiles)
            throws URISyntaxException  {

        TicketDto updated = ticketService.update(dto, multipartFiles);
        return ResponseEntity.ok().location(new URI(request.getRequestURI())).body(updated);
    }

    @PutMapping(value = "/{id}/state")
    public ResponseEntity<TicketDto> update(final HttpServletRequest request,
                                           @RequestBody final ActionDto dto) throws URISyntaxException {
        TicketDto updated = ticketService.updateState(dto);
        return ResponseEntity.ok().location(new URI(request.getRequestURI())).body(updated);
    }
}
