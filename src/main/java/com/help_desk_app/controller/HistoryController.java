package com.help_desk_app.controller;

import com.help_desk_app.history.change.Change;

import com.help_desk_app.history.service.HistoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/history")
public class HistoryController {

    private final HistoryService historyService;

    public HistoryController(HistoryService historyService) {
        this.historyService = historyService;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<List<Change>> getHistory(@PathVariable final Long id) {
        return ResponseEntity.ok(historyService.getHistoryByTicketId(id));
    }

}