package com.help_desk_app.controller;

import com.help_desk_app.dto.FeedbackDto;
import com.help_desk_app.service.FeedbackService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.net.URI;

@RestController
@RequestMapping("/feedback")
public class FeedbackController {

    private final FeedbackService feedbackService;

    public FeedbackController(FeedbackService feedbackService) {
        this.feedbackService = feedbackService;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<FeedbackDto> get(@PathVariable final Long id) {
        return ResponseEntity.ok(feedbackService.findOneByTicketId(id));
    }

    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('MANAGER')")
    @PostMapping
    public ResponseEntity<FeedbackDto> create(@RequestBody final FeedbackDto dto) {
        FeedbackDto created = feedbackService.create(dto);
        final URI uri =
                ServletUriComponentsBuilder.fromCurrentRequest()
                        .path("/{id}")
                        .buildAndExpand(created.getTicketId())
                        .toUri();
        return ResponseEntity.created(uri).body(created);
    }
}
