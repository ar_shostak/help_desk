package com.help_desk_app.controller;

import com.help_desk_app.dto.TicketDto;
import com.help_desk_app.dto.UserDto;
import com.help_desk_app.service.TicketService;
import com.help_desk_app.service.UserService;
import org.hibernate.service.spi.ServiceException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Set;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final TicketService ticketService;

    public UserController(UserService userService, TicketService ticketServic) {
        this.ticketService = ticketServic;
        this.userService = userService;
    }

    @GetMapping("/{id}/tickets")
    public ResponseEntity<Set<TicketDto>> getTickets(@PathVariable Long id) throws ServiceException {
        return ResponseEntity.ok(ticketService.getAllTicketsByUserId(id));
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUser(@PathVariable final Long id) throws ServiceException {
        return ResponseEntity.ok(userService.getUserById(id));
    }
}
