import axios from 'axios'
import { Component } from 'react';

const APPLICATION_API_URL = 'http://localhost:8080/Help_Desk'
const TICKET_API_URL = `${APPLICATION_API_URL}/tickets`
const HISTORY_API_URL = `${APPLICATION_API_URL}/history`
const COMMENTS_API_URL = `${APPLICATION_API_URL}/comments`

class TicketOverviewService extends Component {
    constructor(props) {
        super(props)
        this.getTicket = this.getTicket.bind(this);
    }

    getTicket(props) {
        return axios.get(`${TICKET_API_URL}/${props.ticketId}`,
        {
            withCredentials: true,
            headers: {
                "Authorization" : `Bearer ${props.user.jwt}`,
                "Accept": "application/json",
                "Content-Type": "application/json",
             }
        }
        ).then(response => {return response.data})
        .catch(function (error) {   
            alert('Unable to load ticket')
            return Promise.reject(error)
        });
    }

    getHistory(props) {
        return axios.get(`${HISTORY_API_URL}/${props.ticketId}`,
        {
            withCredentials: true,
            headers: {
                "Authorization" : `Bearer ${props.user.jwt}`,
                "Accept": "application/json",
                "Content-Type": "application/json",
             }
        }
        ).then(response => {return response.data})
        .catch(function (error) {   
            alert('Unable to load history')
            return Promise.reject(error)
        });
    }
    
    getComments(props) {
        return axios.get(`${COMMENTS_API_URL}/${props.ticketId}`, 
        {
            withCredentials: true,
            headers: {
                "Authorization" : `Bearer ${props.user.jwt}`,
                "Accept": "application/json",
                "Content-Type": "application/json",
             }
        }
        ).then(response => {return response.data})
        .catch(function (error) {   
            alert('Unable to load comments')
            return Promise.reject(error)
        });
    }
}

export default new TicketOverviewService()