import { NavLink } from 'react-router-dom';
import React from 'react';
import './CreateBar.css';

const CreateBar = (props) => {
    let isNotEngeneer = () => {
        if(props.user.role === 'ENGINEER') {
            return true;
        }
        return false;
    }

    return(
        <nav className='bar'>
            <div >
                <NavLink to ='/ticketConstructor'>
                    <button disabled = {isNotEngeneer()} className ='createNewTicketButton'>Create New Ticket</button>
                </NavLink>
            </div>
        </nav>
    )
}

export default CreateBar;