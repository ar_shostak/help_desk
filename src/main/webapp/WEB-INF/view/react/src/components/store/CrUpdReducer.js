const SET_NEW_TICKET = 'SET_NEW_TICKET';
const SET_ENUMS = 'SET_ENUMS';

let CrUpdState = {
    enumsMap: {
        categories : [],
        urgency : [],
        rate : []
    },
    newTicket : {},
};

const CrUpdReducer = (state = CrUpdState, action) => {
switch(action.type) {
    case 'SET_NEW_TICKET':  {
        return { 
            ...state,
            newTicket : action.newTicket,    
        }
    }
    case 'SET_ENUMS':  {
        return { 
            ...state,
            enumsMap : action.enumsMap,    
        }
    }
    default :
        return state; 
    }
}

export const setNewTicket = (props) => ({
    type: SET_NEW_TICKET,
    newTicket : props.newTicket })  

export const setEnums = (props) => ({
    type: SET_ENUMS,
    enumsMap : props.enumsMap })

export default CrUpdReducer;