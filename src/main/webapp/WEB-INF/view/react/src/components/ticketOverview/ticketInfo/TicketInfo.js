import React from 'react';
import './TicketInfo.css';

let TicketInfo = (props) => {
        let ticket = props.ticket;
        let displayAttachments = () => {
            if (ticket.attachments !== undefined){
                return (
                    <ul>
                        {ticket.attachments.map((attachment) => {
                            return(
                                <img src={`data:${attachment.contentType};base64,${attachment.blob}`} alt="ticketinfo.attachment" width={150} />
                            ) 
                        })}
                    </ul>
                )
            } 
        }

        return (
            <div className = 'infoTable'>
            <h1>{ticket.ticketName} info</h1>
            <table>
                <tdoby>
                <tr>
                    <td>Created on:</td>
                    <td></td>
                    <td>{ticket.createdOn}</td> 
                </tr>
                <tr>
                    <td>Status:</td>
                    <td></td>
                    <td>{ticket.state}</td>
                    <td>Category:</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Urgency:</td>
                    <td></td>
                    <td>{ticket.urgency}</td>
                    <td>{ticket.category}</td>          
                </tr>
                <tr>
                    <td>Desired resolution date:</td>
                    <td></td>
                    <td>{ticket.desiredResolutionDate}</td>                   
                </tr>
                <tr>
                    <td>Owner:</td>
                    <td></td>
                    <td>{`${ticket.user.firstName} ${ticket.user.lastName}`}</td>                
                </tr>
                <tr>
                    <td>Approver:</td>
                    <td></td>
                    <td>{ticket.approverName}</td>
                </tr>
                <tr>
                    <td>Assignee:</td>
                    <td></td>
                    <td>{ticket.assigneeName}</td>
                  
                </tr>
                <tr>
                    <td>Attachments:</td>
                    <td></td>
                    <td>{displayAttachments()}</td>
                </tr>
                <tr>
                    <td>Description:</td>
                    <td></td>
                    <td>{ticket.description}</td>
                </tr>
                </tdoby>
            </table>
            </div>
        )
}

export default TicketInfo;