const REFRESH_TICKETS = 'REFRESH_TICKETS';
const SET_TICKETS = 'SET_TICKETS';
const SWITCH_IS_FETCHING = 'SWITCH_IS_FETCHING';
const UPDATE_TICKET = 'UPDATE_TICKET';

let tickets = {
    tickets: [],
    button: 'Button1',
    message: null,
    isFetching : false
}

const TicketsReducer = (state = tickets, action) => {
switch(action.type) {
    
    case 'REFRESH_TICKETS' : {
        return { 
            ...state,
            tickets : action.tickets,    
        }
    }
    case 'SET_TICKETS':  {
        return { 
            ...state,
            tickets : action.tickets,    
        }
    }
    case 'SWITCH_IS_FETCHING':  {
        return { 
            ...state,
            isFetching : action.isFetching,    
        }
    }
    case 'UPDATE_TICKET':  {
        const updatedHeaders = state.tickets.map((obj) => {
            return obj.id === action.ticket.id ? action.ticket : obj;
          });
        return {         
            ...state,
            tickets : updatedHeaders    
        }
    }
    default :
        return state; 
    }

}
    export const refreshTickets = (props) => ({
        type: REFRESH_TICKETS,
        tickets: props.tickets })
    export const updateTicket = (props) => ({
        type: UPDATE_TICKET,
        ticket: props.ticket })
    export const setTickets = (props) => ({
        type: SET_TICKETS,
        tickets: props.tickets })

    export const switchIsFetching = (props) => ({
        type: SWITCH_IS_FETCHING,
        isFetching: props.isFetching })    

export default TicketsReducer;