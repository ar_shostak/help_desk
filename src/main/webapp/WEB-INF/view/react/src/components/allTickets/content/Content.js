import React from 'react';
import './Content.css';
import RenderTableData from './RenderTableData.js';

const Content = (props) => {
    return (
        <table className="table">
            <tbody>
                <RenderTableData tickets = {props.tickets}
                                 history = {props.history}
                                 putTicketState = {props.putTicketState}/>
            </tbody>
        </table>
    )
}

export default Content;

