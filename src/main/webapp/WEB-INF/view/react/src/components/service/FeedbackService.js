import axios from 'axios'
import { Component } from 'react';

const APPLICATION_API_URL = 'http://localhost:8080/Help_Desk'
const FEEDBACK_API_URL = `${APPLICATION_API_URL}/feedback`

class FeedbackService extends Component {
    constructor(props) {
        super(props)
        this.getFeedback = this.getFeedback.bind(this);
        this.postFeedback = this.postFeedback.bind(this);
    }

    getFeedback(props) {
        return axios.get(`${FEEDBACK_API_URL}/${props.ticketId}`,
        {
            withCredentials: true,
            headers: {
                "Authorization" : `Bearer ${props.user.jwt}`,
                "Accept": "application/json",
                "Content-Type": "application/json",
             }
        })
        .then(response => {return response.data})
        .catch(error => {
            return Promise.reject(error)
        });    
    }

    postFeedback(props) {
        return axios.post(`${FEEDBACK_API_URL}`, props.feedback, 
        {
            withCredentials: true,
            headers: {
                "Authorization" : `Bearer ${props.user.jwt}`,
                "Accept": "application/json",
                "Content-Type": "application/json",
             }
        })
        .then(response => {return response})
        .catch(error => {
            alert('Unable to leave feedback');
            return Promise.reject(error)
        });
    }
}

export default new FeedbackService()