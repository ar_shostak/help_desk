import React from 'react';

let Comments = (props) => {
    let content = props.content;

    let buildHeader = () => {
        return (<tr>
                <th>Date</th>
                <th>User</th>
                <th>Comment</th>
            </tr>
        )
    };

    let renderTableData = (content) => {
        return content.map(ticket => {
            return (
                <tr key={ticket.id}>
                    <td>{ticket.date}</td>
                    <td>{ticket.user}</td>
                    <td>{ticket.text}</td>
                </tr>
            )
        })
    };
    return (
        <div>
            <table className="table">
                <tbody>
                {buildHeader(content)}
                {renderTableData(content)}
                </tbody>
            </table>
        </div>
    )

};

export default Comments;