import React from 'react';
import CreateBar from './createBar/CreateBar';
import SwitchBar from './switchBar/SwitchBar';
import TicketsFilter from './ticketsFilter/TicketsFilter';

const Header = (props) => {
    return (    
        <div>
            <CreateBar user = {props.user}/>
            <SwitchBar refreshTickets = {props.refreshTickets}
                       onAllTickets = {props.match.isExact} />               
            <TicketsFilter />
        </div>
    )
}

export default Header;