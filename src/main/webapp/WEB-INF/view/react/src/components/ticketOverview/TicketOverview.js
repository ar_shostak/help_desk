import React from 'react';
import OverviewHeader from './overviewHeader/OverviewHeader';
import OverviewSwitchBar from './overviewSwitchBar/OverviewSwitchBar';
import OverviewContent from './overviewContent/OverviewContent'

let TicketOverview = (props) => {
    return(
        <div>
            <div><OverviewHeader ticket = {props.ticketOverviewPage.ticket} isOwner = {props.isOwner} isPresent = {props.isPresent} comments = {props.comments}/></div>
            <div><OverviewSwitchBar ticketId = {props.ticketId}
                                    getComments = {props.getComments}
                                    getHistory = {props.getHistory}
                                    comments = {props.comments}/></div>
            <div><OverviewContent content = {props.ticketOverviewPage.content}
                                  comments = {props.comments} /></div>
        </div>
    )
}

export default TicketOverview;