import React from 'react';
import Content from './content/Content.js';
import Header from './header/Header.js';
import { Route } from 'react-router-dom';

let Tickets = (props) => {
    let refreshTickets = props.refreshTickets;
    let tickets = props.tickets;
    let putTicketState = props.putTicketState;
    let user = props.user;
    return (
        <div>
            <div>
                <Route render = {(props)=> <div><Header {...props} refreshTickets = {refreshTickets} user = {user}/></div>} />
            </div>
            <div>
                <Route exact path='/myTickets'  render = {(props)=> <div><Content {...props} tickets = {tickets} putTicketState = {putTicketState}/></div>} /> 
                <Route path='/' render = {(props)=> <div><Content {...props} tickets = {tickets} putTicketState = {putTicketState}/></div>} /> 
            </div>
        </div>
    )
}

export default Tickets;