import React from 'react';
import {NavLink} from 'react-router-dom';
import './OverviewHeader.css';
import TicketInfo from './../ticketInfo/TicketInfo';

const OverviewHeader = (props) => {
    let choseButton = () => {
        return (
            props.comments === undefined ?
                leaveFeedback() : viewFeedback()
        )
    };

    let leaveFeedback = () => {
        return (
            <NavLink to = {{pathname : `/feedbacks/${props.ticket.id}/create`, state : { ticketName : props.ticket.user.id}}}>
                <button disabled = {(!props.isOwner) || props.isPresent || props.ticket.state !== 'DONE' } className='overviewButton'>Leave Feedback</button>
            </NavLink>
        )
    };

    let viewFeedback = () => {
        return (
            <NavLink to = {{pathname : `/feedbacks/${props.ticket.id}`, state : { ticketName : props.ticket.ticketName}}}>
                <button className='overviewButton'>Feedback</button>
            </NavLink>
        )
    };

    return (
        <nav className='headBar'>
            <table className='overviewButtonsMaket'>
                <td className='overviewButtonsLeftcol'>
                    <NavLink to='/tickets'>
                        <button className='overviewButton'>Ticket List</button>
                    </NavLink>
                </td>
                <td></td>
                <td >
                    <NavLink to = {`/ticketConstructor/${props.ticket.id}`} ticket={props.ticket}>
                        <button disabled = {!props.isOwner} className='overviewButton'>Edit</button>
                    </NavLink>
                    {choseButton()}
                </td>
            </table>
            <div>
                <TicketInfo ticket={props.ticket}/>
            </div>
        </nav>
    )
};

export default OverviewHeader;