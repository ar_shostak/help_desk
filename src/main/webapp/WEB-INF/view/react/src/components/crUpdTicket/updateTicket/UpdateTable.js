import React from 'react';
import {TextArea, Select, FieldFileInput} from '../../utils/CustomTags';
import { Field, reduxForm } from 'redux-form';
import {required, maxLengthCreator} from '../../validator/CrUpdValidator';

const maxLength100 = maxLengthCreator(100);
const maxLength500 = maxLengthCreator(500);

let UpdateTable = (props) => {
    const handleSubmit = (formData) => {
        const data = new FormData();
        let ticket = {
            id : props.ticketId,
            category : formData.category,
            ticketName : formData.ticketName,
            description : formData.description,
            urgency : formData.urgency, 
            desiredResolutionDate : formData.desiredResolutionDate,
            comment : formData.comment,
            state : formData.state
            }
            data.append("ticket", JSON.stringify(ticket));

            let attachments = formData.image;
            if (attachments === undefined) {
                attachments = [];
            }
   
            let fromBase64 = (b64Data, contentType, sliceSize) => {
                contentType = contentType || '';
                sliceSize = sliceSize || 512;
        
                var byteCharacters = atob(b64Data);
                var byteArrays = [];
        
                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);
        
                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }
        
                    var byteArray = new Uint8Array(byteNumbers);
        
                    byteArrays.push(byteArray);
                }
        
              var blob = new Blob(byteArrays, {type: contentType});
              return blob;
        }

            let existingFiles = props.currentTicket.attachments;
                for(let i = 0; i < existingFiles.length; i++){
                    let decoded = fromBase64(existingFiles[i].blob, existingFiles[i].contentType, existingFiles[i].blob.length);
                    attachments.push(new File([decoded],
                                            `${existingFiles[i].name}`,
                                            {type : existingFiles[i].contentType, lastModified:new Date()}));
                }
                for(let i = 0; i < attachments.length; i++){
                    data.append(`attachments`, attachments[i], `${attachments[i].name}`);
                }
            props.putTicket(data);  
        } 

    return(
        <div>
          <UpdateTicketReduxForm onSubmit = {handleSubmit}
                                 currentTicket = {props.currentTicket}
                                 setTicket = {props.setTicket}
                                 enumsMap = {props.enumsMap}/>
        </div>
    )
}

const UpdateTicketForm = (props) => {
let ticket = props.currentTicket;

let deleteImage = (attachmentId) => {
    let files = ticket.attachments;
    for(let i = 0; i < files.length; i++){
        if(files[i].id === attachmentId)
        files.splice(i, 1);
    }
    props.setTicket({ticket : { 
        ...props.currentTicket,
        attachments : files    
    }});
}

let chosenFiles = () => {
    if (ticket.attachments !== undefined){
        return (
            <ul>
                {ticket.attachments.map(attachment => {
                     return(
                        <div className = 'img-wraps'><button id="clear" className="closes" type = 'button' onClick=
                        {e => deleteImage(attachment.id)}>x</button>
                        <img src = {`data:${attachment.contentType};base64,${attachment.blob}`} className = "hover" alt="attachment img" width={100} />   
                        </div> 
                    )
                })}
            </ul>
        )
    } 
}

    return(
        <form>
            <div><Field placeholder = {`${ticket.category}`} name = {'category'}
                        component = {Select} default = {`${ticket.category}`} enum = {props.enumsMap.categories} validate = {[required]}/></div>
            <div><Field placeholder = {`${ticket.ticketName}`} name = {'ticketName'}
                        component = {TextArea} className='ticketInput' validate = {[required, maxLength100]}/></div>   
            <div><Field placeholder = {`${ticket.description === undefined ? 'Description' : ticket.description}`} name = {'description'}
                        component = {TextArea} className='ticketInput' validate = {[maxLength500]}/></div>  
            <div><Field placeholder = {`${ticket.urgency}`} name = {'urgency'} validate = {[required]}
                        component = {Select} enum = {props.enumsMap.urgency} /></div>  
            <div><Field placeholder = {`${ticket.desiredResolutionDate}`} name = {'desiredResolutionDate'}
                        component = {TextArea} className='ticketInput'/></div>  
            <div>{chosenFiles()}</div>
            <div><Field name = {'image'} component = {FieldFileInput} type = {'file'} label = {'Browse'}  className = {'file'}/></div>
            <div><Field placeholder = {`${ticket.comment === undefined ? 'Comment' : ticket.comment}`} name = {'comment'}
                        component = {TextArea} className='ticketInput' validate = {[maxLength500]}/></div>   
            <div>
                    <button onClick={props.handleSubmit(values => props.onSubmit({ 
                        ...values, state: 'DRAFT' }))} className='overviewButton'>Save as Draft</button>
                    <button onClick={props.handleSubmit(values => props.onSubmit({ 
                        ...values, state: 'NEW' }))} className='overviewButton' >Submit</button>  
            </div>
        </form>
    )
}

const UpdateTicketReduxForm = reduxForm( { form : 'updateTicket' } ) (UpdateTicketForm)
    
export default UpdateTable;
