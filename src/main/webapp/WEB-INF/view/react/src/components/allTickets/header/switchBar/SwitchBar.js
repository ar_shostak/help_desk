import { NavLink } from 'react-router-dom';
import React from 'react';

const SwitchBar = (props) => {
    let lockButton = () => {
        if(props.onAllTickets !== undefined && document.getElementById('Button1') !=null) {
            if (props.onAllTickets === true ){
                document.getElementById('Button1').disabled = true;
                document.getElementById('Button2').disabled = false;
            } else {
                document.getElementById('Button1').disabled = false;
                document.getElementById('Button2').disabled = true;
            }
        }
    }
    
    let renderAllTickets = () => {
        props.refreshTickets({action : 'ALL'})
    }

    let renderMyTickets = () => {
        props.refreshTickets({action : 'MY'});
    }

    lockButton() 
    return(
        <nav className='switchBar'>
            <div >
                <NavLink to ='/tickets'>
                    <button onClick = {renderAllTickets} id = 'Button1' className ='button'>All Tickets</button>
                </NavLink>
                <NavLink to ='/tickets/myTickets'>
                    <button onClick = {renderMyTickets} id = 'Button2' className ='button'>My Tickets</button>
                </NavLink >
            </div>
        </nav>
    )
}

export default SwitchBar;