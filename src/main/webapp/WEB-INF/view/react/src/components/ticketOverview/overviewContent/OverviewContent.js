import React from 'react';
import History from "./History";
import Comments from "./Comments";

let OverviewContent = (props) => {
    let component = (props.comments === undefined ? ()=>{return (<History content = {props.content}/>)}
                                                  : ()=>{return (<Comments content = {props.content}/>)});
        return (
            <div>
                {component()}
            </div>
        )
};

export default OverviewContent;