import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { setFeedback } from '../store/FeedbackReducer';
import { setTicket, switchIsFetching, setContent } from '../store/TicketOverviewReducer';
import Preloader from '../preloader/Preloader';
import TicketOverviewService from '../service/TicketOverviewService';
import FeedbackService from '../service/FeedbackService'
import TicketOverview from './TicketOverview';

class TicketOverviewContainer extends Component {

    constructor(props){
        super(props);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.getContent = this.getContent.bind(this);
        this.getHistory = this.getHistory.bind(this);
        this.getComments = this.getComments.bind(this);
        this.isOwner = this.isOwner.bind(this);
        this.isFeedbackPresent = this.isFeedbackPresent.bind(this);
    }

    isOwner = () =>{
        if(this.props.user.id === this.props.ticketOverviewPage.ticket.user.id) {
            return true;
        }
        return false;
    }
    
    isFeedbackPresent = () =>{
        if(this.props.feedbackPage.feedback.rate !== undefined) {
            return true;
        }
        return false;
    }

    getFeedback(){
        this.props.switchIsFetching({isFetching : true});  
        FeedbackService.getFeedback({ticketId : this.props.match.params.ticketId,
                                     user : this.props.user}).then(data => {
            if(data === null) {
                this.props.setFeedback({feedback : []});
            }
            this.props.setFeedback({feedback : data});
            this.props.switchIsFetching({isFetching : false});
        });
    }

    getHistory(ticketId){
        this.props.switchIsFetching({isFetching : true});    
            TicketOverviewService.getHistory({ticketId : this.props.match.params.ticketId,
                                              user : this.props.user}).then(data => {
            this.props.setContent({content : data});
            this.props.switchIsFetching({isFetching : false});
        });
    }

    getComments(ticketId){
        this.props.switchIsFetching({isFetching : true});    
            TicketOverviewService.getComments({ticketId : this.props.match.params.ticketId,
                                               user : this.props.user}).then(data => {
            this.props.setContent({content : data});
            this.props.switchIsFetching({isFetching : false});
        });
    }
    
    getContent(ticketId){
        if (this.props.match.params.comments === undefined) {
            this.getHistory(ticketId);
        } else {
            this.getComments(ticketId);
        }
    };

    componentDidMount(){
        this.props.switchIsFetching({isFetching : true});
        TicketOverviewService.getTicket({ticketId : this.props.match.params.ticketId,
                                         user : this.props.user}).then(data => {
            this.props.setTicket({ticket : data});
            this.getContent(this.props.match.params.ticketId)
            this.getFeedback();
            this.props.switchIsFetching({isFetching : false});
        });   
    };

    render() {
        return(<div> {this.props.ticketOverviewPage.isFetching ? <Preloader/> : null}
                    <TicketOverview ticketOverviewPage = {this.props.ticketOverviewPage}
                                    isOwner = {this.isOwner()} 
                                    isPresent = {this.isFeedbackPresent()}                               
                                    ticketId = {this.props.match.params.ticketId}
                                    getHistory = {this.getHistory}
                                    getComments = {this.getComments}
                                    comments = {this.props.match.params.comments}/> 
                </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        ticketOverviewPage: state.ticketOverviewPage,
        feedbackPage: state.feedbackPage,
        user: state.loginPage.user
    }
}

let WithRouteTicketOverviewContainer = withRouter(TicketOverviewContainer);
  
export default connect(mapStateToProps, {
    setTicket, switchIsFetching, setContent, setFeedback,
    })(WithRouteTicketOverviewContainer);
