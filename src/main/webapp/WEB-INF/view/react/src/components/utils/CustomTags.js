import React, {Component} from 'react';
import './CustomTags.css';
   
    export const Select = ({input, meta, ...props}) => {
        const isCorrect = meta.touched && meta.error;

        let renderSelectOptions = (unit) => (
            <option key={unit} value={unit}>{unit}</option>
          )
        return(
            <div className = {isCorrect ? 'error' : 'valid'}>
                <select {...input}>
                    <option disabled selected hidden value="">{props.placeholder === 'undefined' ? Select : props.placeholder}</option>
                    {props.enum.map(renderSelectOptions)} 
                </select>
                {isCorrect && <span>{meta.error}</span>}
            </div>
        )
    }

    export const TextArea = ({input, meta, ...props}) => {
        const isCorrect = meta.touched && meta.error;
        return(
            <div className = {isCorrect ? 'error' : 'valid'}>
                <div><textarea {...input} {...props} /></div>
                {isCorrect && <span>{meta.error}</span>}
            </div>
        )
    }

export class FieldFileInput  extends Component{
  constructor(props) {
    super(props)
    this.state = {chosenFiles : []};
    this.onChange = this.onChange.bind(this);
    this.deleteFile = this.deleteFile.bind(this);
  }
 
    onChange(e) {
        const { input: { onChange } } = this.props
        let files = [
            ...this.state.chosenFiles,
            ...e.target.files]
        this.setState({chosenFiles : files})
        onChange(files);
    }

    deleteFile = (name) => {
        const { input: { onChange } } = this.props
        let files = [...this.state.chosenFiles];
        for(let i = 0; i < files.length; i++){
            if(files[i].name === name) {
                files.splice(i, 1);
            }
        }
        this.setState({chosenFiles : files});
        onChange(files);
}

  render(){
    const {label} = this.props      
    return(
        <div>
            {this.state.chosenFiles.map(file => <div>
                                                    <li>{file.name}</li>
                                                    <button type = 'button' onClick = {e => this.deleteFile(file.name)}>x</button>
                                                </div>)}
            <label className = 'label' for = "file">{label}</label>
            <input
                id = "file"
                type='file'
                name="file"
                className ="inputfile"
                accept='.jpg, .png, .jpeg, .pdf, .doc, .docx'
                multiple
                onChange={this.onChange}/>
        </div>
    )
  }
}
    