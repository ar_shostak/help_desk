import React from 'react';
import HistoryComponent from './../../utils/HistoryComponent';

let History = (props) => {
         let buildHeader = () => {
            return (<tr>
                <th>Date</th>
                <th>User</th>
                <th>Action</th>
                <th>Description</th>
                </tr>
           )
        };

        let renderTableData = () => {
            return props.content.map(revision => {
                let data = HistoryComponent(revision)
                return (
                    <tr key={revision.id}>
                        <td>{revision.date}</td>
                        <td>{revision.userName}</td>
                        <td>{data.action}</td>
                        <td>{data.description}</td>
                    </tr>
                )
            })
        };

        return (
            <div>
                <table className="table">
                    <tbody>
                    {buildHeader()}
                    {renderTableData()}
                    </tbody>
                </table>
            </div>
        )

};

export default History;