const CREATION = 'CREATION';
const STATUS_CHANGE = 'STATUS_CHANGE';
const ATTACHMENT_ADD = 'ATTACHMENT_ADD';
const ATTACHMENT_REMOVE = 'ATTACHMENT_REMOVE';

let getHistoryDate = (props) => {
    
      let data = {
        action : '',
        description : ''
    }

    switch(props.type) {
        case CREATION:
            data.action = 'Ticket is created';
            data.description = data.action;
            break;
        case STATUS_CHANGE:
            data.action = 'Ticket status is changed';
            data.description = 'Ticket status is changed from %old to %new'.replace('%old', props.originValue).replace('%new', props.newValue);
            break;
        case ATTACHMENT_ADD:
            data.action = 'Files is attached';
            data.description = 'Files is attached: %new'.replace('%new', props.newValue);
            break;
        case ATTACHMENT_REMOVE:
            data.action = 'File is removed';
            data.description = 'File is removed: %old'.replace('%old', props.originValue);
            break;
        default:
            data.action = 'Ticket is edited';
            data.description = data.action;
            break;
        }
        return data;
    }

export default getHistoryDate;