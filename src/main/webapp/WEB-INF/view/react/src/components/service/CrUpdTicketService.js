import axios from 'axios'
import { Component } from 'react';

const APPLICATION_API_URL = 'http://localhost:8080/Help_Desk'
const TICKET_API_URL = `${APPLICATION_API_URL}/tickets`

class CrUpdTicketService extends Component {
    constructor(props) {
        super(props)
        this.getTicket = this.getTicket.bind(this);
        this.postTicket = this.postTicket.bind(this);
        this.putTicket = this.putTicket.bind(this);
    }

    getTicket(props) {
        return axios.get(`${TICKET_API_URL}/${props.id}`, {withCredentials: true,
            headers: {
                "Authorization" : `Bearer ${props.user.jwt}`,
                "Accept": "application/json",
                "Content-Type": "application/json",
            }
        })
        .then(response => {return response.data})
        .catch(error => {   
            alert('Unable to load ticket')
            return Promise.reject(error)
        });
    }

    getEnums(props) {
        return axios.get(`${APPLICATION_API_URL}/common`, {withCredentials: true,
            headers: {
                "Authorization" : `Bearer ${props.user.jwt}`,
                "Accept": "application/json",
                "Content-Type": "application/json",
            }
        }) 
        .then(response => {return response.data})
        .catch(error => {  
            alert('Unable to load content')
            return Promise.reject(error)
        });
    }

    postTicket(props) {
        return axios.post(`${TICKET_API_URL}`, props.ticket, {withCredentials: true,
            headers: {
                "Authorization" : `Bearer ${props.user.jwt}`,
                "Content-Type": undefined
            }
        })
        .then(response => {if (response.status === 201) {return response.data}})
        .catch(error => {
            alert('Unable to create ticket')
            return Promise.reject(error)
       });
    }

    putTicket(props) {
        return axios.put(`${TICKET_API_URL}/${props.ticket.id}`, props.ticket, {withCredentials: true,
            headers: {
                "Authorization" : `Bearer ${props.user.jwt}`,
                "Content-Type": undefined
            }
        })
        .then(response => {if (response.status === 200) {return response.data}})
        .catch(error => {
            alert('Unable to update ticket')
            return Promise.reject(error)
       });
    }


putTicketState(props) {
    return axios.put(`${TICKET_API_URL}/${props.ticketState.id}/state`, props.ticketState, {withCredentials: true,
        headers: {
            "Authorization" : `Bearer ${props.user.jwt}`,
            "Accept": "application/json",
            "Content-Type": "application/json",
        }
    })
    .then(response => {if (response.status === 200) {return response.data}})
    .catch(error => {
        alert('Unable to do action')
        return Promise.reject(error)
   });
}
}

export default new CrUpdTicketService()