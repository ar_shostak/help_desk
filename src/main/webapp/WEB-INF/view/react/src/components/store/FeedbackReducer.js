const SET_FEEDBACK = 'SET_FEEDBACK';

let feedbackState = {
    feedback: {}
}

const FeedbackReducer = (state = feedbackState, action) => {
switch(action.type) {
    case 'SET_FEEDBACK':  {
        return { 
            ...state,
            feedback : action.feedback,    
        }
    }
    default :
        return state; 
    }
}

export const setFeedback = (props) => ({
    type: SET_FEEDBACK,
    feedback : props.feedback })
    
export default FeedbackReducer;