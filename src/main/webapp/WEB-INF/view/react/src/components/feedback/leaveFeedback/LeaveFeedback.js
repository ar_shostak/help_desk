import React from 'react';
import { Field, reduxForm } from 'redux-form';
import {required, maxLengthCreator} from '../../validator/CrUpdValidator';
import {Select, TextArea} from '../../utils/CustomTags';

const maxLength500 = maxLengthCreator(500);

const LeaveFeedback = (props) => {
    const handleSubmit = (formData) => {
        props.postFeedback( 
            {
            ticketId : props.ticketId,
            rate : formData.feedbackRate,
            text : formData.feedbackText,
            }
        )
    }

    return(
        <div>
          <h3>{props.ticketName}</h3>
          <h4>Feedback</h4>
          <FeedbackReduxForm onSubmit = {handleSubmit} rate = {props.rate}/>
        </div>
    )
}

const FeedbackForm = (props) => {
    return(
        <form onSubmit = {props.handleSubmit} >
            <div>
                <Field placeholder = {'Rate'} name = {'feedbackRate'}
                       validate = {[required]} component = {Select} enum = {props.rate}/>
            </div>
            <div>
                <Field placeholder = {'There is some feedback about an Admin work with the ticket'} 
                       name = {'feedbackText'} component = {TextArea} validate = {[maxLength500]}/>
            </div>   
            <div>
                <button className='overviewButton'>Submit</button>
            </div>
        </form>
    )
}

const FeedbackReduxForm = reduxForm( { form : 'feedback' } ) (FeedbackForm)

export default LeaveFeedback;