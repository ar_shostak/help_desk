import React from 'react';
import {NavLink} from 'react-router-dom';
import { Field, reduxForm} from 'redux-form';
import {Select} from '../../utils/CustomTags';

let RenderTableData = (props) => {

    const handleSubmit = (formData) => {
        switch(formData.action) {
            case 'Leave Feedback':
                props.history.push({
                    pathname: `/feedbacks/${formData.id}/create`,
                    state: {id : formData.id,
                        ticketName : formData.ticketName}
                })
                break;
            case 'View Feedback':
                props.history.push({
                    pathname: `/feedbacks/${formData.id}`,
                    state: {id : formData.id,
                        ticketName : formData.ticketName}
            })
                break;
            default:
                props.putTicketState( {id : formData.id,                    
                                  action : formData.action} )
        }
    }

    let header = () => {
        return (
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Desired Date</th>
                <th>Urgency</th>
                <th>Status</th>
                <th className = 'actionTh'>Action</th>
            </tr>
        )
    };


    let body = (props) => {
        return (
            props.tickets.map(
                ticket =>
                    <tr key={ticket.id}>
                        <td>{ticket.id}</td>
                        <td><NavLink to={'/ticket/' + ticket.id}>{ticket.ticketName}</NavLink></td>
                        <td>{ticket.desiredResolutionDate}</td>
                        <td>{ticket.urgency}</td>
                        <td>{ticket.state}</td>
                        <td><DoActionReduxForm className = 'actionColumn' onSubmit = {handleSubmit}
                                               form = {`doAction.${ticket.id}`}
                                               ticketName = {ticket.ticketName}
                                               id = {ticket.id}
                                               actions = {ticket.actions}/></td>
                    </tr>
            )
        )
    }

    return (
        <div>
            {header()}
            {body(props)}
        </div>
    )
}

    const DoActionForm = (props) => {
        return(
            <form className = 'actionForm'>
                <div><Field placeholder = {`select action`} name = {`action`}
                            component = {Select} enum = {props.actions}  id = {props.id} ticketName = {props.ticketName}/></div>
                <div><button onClick={props.handleSubmit(values => props.onSubmit({ 
                        ...values, ticketName : props.ticketName, id : props.id}))} className='actionButton'>{'\u2713'}</button></div>
                    
            </form>
        )
    }
    
    const DoActionReduxForm = reduxForm()(DoActionForm);

export default RenderTableData;