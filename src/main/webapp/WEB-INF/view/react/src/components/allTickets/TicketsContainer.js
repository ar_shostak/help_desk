import React, {Component} from 'react';
import './content/Content.css';
import { withRouter } from 'react-router-dom';
import Preloader from '../preloader/Preloader';
import CrUpdTicketService from './../service/CrUpdTicketService';
import Tickets from './Tickets';
import {connect} from 'react-redux';
import {refreshTickets, switchIsFetching, setTickets, updateTicket} from './../store/TicketsReducer'
import TicketsService from './../service/TicketsService';

class TicketsContainer extends Component {

    constructor(props){
        super(props);
        this.refreshTickets=this.refreshTickets.bind(this);
        this.componentDidMount=this.componentDidMount.bind(this);
        this.putTicket = this.putTicket.bind(this);
        this.putTicketState = this.putTicketState.bind(this);
    }

    componentDidMount(){
        this.props.switchIsFetching({isFetching : true});
        TicketsService.retrieveAllTickets(this.props.user).then(data => {
            this.props.setTickets({tickets : data});
            this.props.switchIsFetching({isFetching : false});
        });
    }

    refreshTickets(props) {
        let retrieve;
        if (props.action === 'ALL') {
            retrieve = TicketsService.retrieveAllTickets;
        } else {
            retrieve = TicketsService.retrieveUserTickets;
        }
        this.props.switchIsFetching({isFetching : true});
        retrieve(this.props.user).then(data => {
            this.props.refreshTickets({tickets : data});
            this.props.switchIsFetching({isFetching : false});
        });
    }

    putTicket(newTicket) {
        this.props.switchIsFetching({isFetching : true});  
        CrUpdTicketService.putTicket({ticket : newTicket, 
                                      user : this.props.user}).then(data => {
            this.props.switchIsFetching({isFetching : false});
        });
    }

    putTicketState(newTicketState) {
        this.props.switchIsFetching({isFetching : true});  
        CrUpdTicketService.putTicketState({ticketState : newTicketState, 
                                      user : this.props.user}).then(data => {
                                      this.props.updateTicket({ticket : data});
            this.props.switchIsFetching({isFetching : false});
        });
    }

    render() {
        return( <div> {this.props.ticketsPage.isFetching ? <Preloader/> : null}
            <Tickets refreshTickets = {this.refreshTickets}
                     user = {this.props.user}
                     putTicketState = {this.putTicketState}
                     tickets = {this.props.ticketsPage.tickets}/>
               </div>      
        )
    }
}

let mapStateToProps = (state) => {
    return {
        ticketsPage: state.ticketsPage,
        user: state.loginPage.user,
        isFetching : state.ticketsPage.isFetching,
    }
}

let WithRouteTicketsContainer = withRouter(TicketsContainer);

export default connect(mapStateToProps, {
    refreshTickets, setTickets, switchIsFetching, updateTicket,
    })(WithRouteTicketsContainer);
