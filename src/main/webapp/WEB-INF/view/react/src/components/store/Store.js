import TicketsReducer from './TicketsReducer';
import TicketOverviewReducer from './TicketOverviewReducer';
import FeedbackReducer from './FeedbackReducer';
import CrUpdReducer from './CrUpdReducer';
import LoginReducer from './LoginReducer';
import {createStore, combineReducers} from 'redux';
import { reducer as formReducer } from 'redux-form';
import {loadState, saveState} from './LocalStorage';

let reducers = combineReducers({
   ticketsPage: TicketsReducer,
   ticketOverviewPage: TicketOverviewReducer,
   feedbackPage : FeedbackReducer,
   crUpdTicketPage : CrUpdReducer,
   loginPage : LoginReducer,
   form : formReducer
});

const persistedState = loadState();

let store = createStore(reducers, persistedState);

store.subscribe(() => {
  saveState({
    loginPage: store.getState().loginPage
  });
});



export default store;