import React from 'react';
import { NavLink } from 'react-router-dom';
import './OverviewSwitchBar.css';

let OverviewSwitchBar = (props) => {

    let historyClicked = () => {
        props.getHistory(props.ticketId);
    }

    let commentsClicked = () => {
        props.getComments(props.ticketId);
     }
    return (
        <nav className='overviewSwitchwitchBar'>
                <NavLink to = {`/ticket/${props.ticketId}`}>
                    <button onClick = {historyClicked} id = 'historyButton' className = 'button' disabled ={props.comments === undefined ? true : false } >History</button>
                </NavLink>
                <NavLink to = {`/ticket/${props.ticketId}/comments`}>
                    <button onClick = {commentsClicked} id = 'commentsButton' className = 'button' disabled ={props.comments === undefined ? false : true} >Comments</button>
                </NavLink>
            <div className = 'tableName'>Show All</div>
        </nav>
    )
}

export default OverviewSwitchBar;