import axios from 'axios'
import { Component } from 'react';

const APPLICATION_API_URL = 'http://localhost:8080/Help_Desk'

class TicketsService extends Component {
    constructor(props) {
        super(props)
        this.retrieveAllTickets=this.retrieveAllTickets.bind(this);
        this.retrieveUserTickets=this.retrieveUserTickets.bind(this);
    }

    retrieveAllTickets(user) {
        return axios.get(`${APPLICATION_API_URL}/tickets`,
            {
                withCredentials: true,
                headers: {
                    "Authorization" : `Bearer ${user.jwt}`,
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                 }
            }
        ).then(response => {return response.data})
        .catch(error => {   
            alert('Unable to load tickets')
            return Promise.reject(error)
        });
    }

    retrieveUserTickets(user) {
        return (axios.get(`${APPLICATION_API_URL}/users/${user.id}/tickets`,
            {
                withCredentials: true,
                headers: {
                    "Authorization" : `Bearer ${user.jwt}`,
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                }

            }
            )).then(response => {return response.data})
            .catch(function (error) {   
                alert('Unable to load tickets')
                return Promise.reject(error)
            });
    }
}

export default new TicketsService()