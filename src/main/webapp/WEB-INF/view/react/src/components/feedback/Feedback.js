import React from 'react';
import BackButton from './backButton/BackButton';
import LeaveFeedback from './leaveFeedback/LeaveFeedback';
import ViewFeedback from './viewFeedback/ViewFeedback';

let Feedback = (props) => {
    let choseButton = () => {
        let returnLeave = () => {return <LeaveFeedback ticketName = {props.ticketName}
                                                       ticketId = {props.ticketId}
                                                       rate = {props.rate}
                                                       postFeedback = {props.postFeedback}/>}
        let returnView = () => {return <ViewFeedback ticketName = {props.ticketName}
                                                     feedback = {props.feedback}
                                                     ticketId = {props.ticketId}/>}
       
       return (
            <div> 
                {props.create === undefined ? returnView() : returnLeave()}
            </div>
        )   
    }   

    return (
        <div>
            <div>
                <BackButton goBack = {props.goBack}/>
            </div>
            <div>
                {choseButton()}
            </div>
        </div>

    )
}

export default Feedback;
