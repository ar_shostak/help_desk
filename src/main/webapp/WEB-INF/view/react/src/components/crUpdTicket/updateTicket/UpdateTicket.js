import React from 'react';
import {NavLink} from 'react-router-dom';
import UpdateTable from './UpdateTable';

const UpdateTicket = (props) => {
    return(
        <div>
            <div>
                <div className='overviewButtonsLeftcol'>
                    <NavLink to = {`/ticket/${props.ticketId}`} >
                        <button className='overviewButton'>Ticket Overview</button>
                    </NavLink>
                </div>
            </div>
            <h4>Edit Ticket</h4>
            <div><UpdateTable enumsMap = {props.enumsMap}
                                  ticketId = {props.ticketId}
                                  setTicket = {props.setTicket}
                                  currentTicket = {props.currentTicket}
                                  putTicket = {props.putTicket}/></div>
        </div>
    )
}

export default UpdateTicket;