import React from 'react';

let ViewFeedback = (props) => {
    return (
        <div>
            <h3>{props.ticketName}</h3>
            <h3>Feedback for this ticket:</h3>
            <h4>{props.feedback.rate}</h4>
            <h4>{props.feedback.text}</h4>
        </div>
    )
}

export default ViewFeedback;