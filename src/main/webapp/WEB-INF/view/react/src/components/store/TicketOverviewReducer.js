const SET_TICKET = 'SET_TICKET';
const SWITCH_IS_FETCHING = 'SWITCH_IS_FETCHING';
const SET_CONTENT = 'SET_CONTENT';

let ticketOverviewState = {
    selectedTicketId : '',
    content : [],
    ticket: {user : { id : '' }   }
}

const TicketOverviewReducer = (state = ticketOverviewState, action) => {
switch(action.type) {
    case 'SWITCH_IS_FETCHING':  {
        return { 
            ...state,
            isFetching : action.isFetching,    
        }
    }
    case 'SET_TICKET' : {
        return { 
            ...state,
            ...state.ticket,
            ticket : action.ticket
        }
    }
    case 'SET_CONTENT' : {
        return { 
            ...state,
            ...state.content,
            content: action.content,
        }
    }
    default :
        return state; 
    }
}

export const setTicket = (props) => ({
    type: SET_TICKET,
    ticket : props.ticket })

export const setContent = (props) => ({
    type: SET_CONTENT,
    content : props.content })

export const switchIsFetching = (props) => ({
    type: SWITCH_IS_FETCHING,
    isFetching: props.isFetching })       
   
export default TicketOverviewReducer;