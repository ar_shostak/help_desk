import React, { Component } from 'react';
import Feedback from './Feedback';
import FeedbackService from '../service/FeedbackService'
import CrUpdTicketService from '../service/CrUpdTicketService';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { switchIsFetching } from '../store/TicketOverviewReducer';
import { setEnums } from '../store/CrUpdReducer';
import { setFeedback } from '../store/FeedbackReducer';
import Preloader from '../preloader/Preloader';

class FeedbackContainer extends Component {

    constructor(props){
        super(props);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.postFeedback = this.postFeedback.bind(this);
        this.getFeedback = this.getFeedback.bind(this);
    }

    componentDidMount(){
        this.props.switchIsFetching({isFetching : true});  
        CrUpdTicketService.getEnums({user : this.props.user}).then(data => {
            this.props.setEnums({enumsMap : data})
        });
        this.getFeedback();
        this.props.switchIsFetching({isFetching : false});
    }

    getFeedback(){
        if (this.props.feedbackPage.feedback === {}) {
            FeedbackService.getFeedback({ticketId : this.props.match.params.ticketId,
                                     user : this.props.user}).then(data => {
                if(data === null) {
                    this.props.setFeedback({feedback : []});
                }
            this.props.setFeedback({feedback : data});
            });
        }
    }

    postFeedback(feedback){
        this.props.switchIsFetching({isFetching : true});   
            FeedbackService.postFeedback({feedback : feedback,
                                          user : this.props.user}).then(response => {
                if (response.status === 201) {
                    this.props.setFeedback({feedback : response.data});
                    this.props.history.push(`/tickets`)
                }
            this.props.switchIsFetching({isFetching : false});
        });
    }

    render() {
        return (<div>{this.props.isFetching ? <Preloader/> : null}
            <Feedback create = {this.props.match.params.create}
                      feedback = {this.props.feedbackPage.feedback}
                      rate = {this.props.enumsMap.rate}
                      ticketId = {this.props.match.params.ticketId}
                      ticketName = {this.props.location.state.ticketName}
                      postFeedback = {this.postFeedback}
                      goBack = {this.props.history.goBack}/>
                </div>
        )
    }
}

let WithRouteFeedbackContainer = withRouter(FeedbackContainer);

let mapStateToProps = (state) => {
    return {
        enumsMap : state.crUpdTicketPage.enumsMap,
        feedbackPage: state.feedbackPage,
        user: state.loginPage.user
    }
}

export default connect(mapStateToProps, {
    setFeedback, switchIsFetching, setEnums,
    })(WithRouteFeedbackContainer);
