import React from 'react';

let BackButton = (props) => {

    let onClick = () => {
        props.goBack()
    }

    return(
        <div >
            <button onClick = {onClick} className='overviewButton' >Back</button>
        </div>
    )
}

export default BackButton;